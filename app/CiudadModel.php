<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CiudadModel extends Model
{
    //
    protected $table="Ciudad";
    /*
"OID" serial NOT NULL,
  "DscaCiudad" character varying(40),
  "IdCanton" integer,
  "IsDefault" boolean,
  "Bandera" bytea,
  "OldCode" integer,
  "OptimisticLockField" integer,
  "GCRecord" integer,
    */
}
