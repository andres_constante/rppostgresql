<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecaudacionModel extends Model
{
    //
    protected $table = 'pagos';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                //'cedula'=>'required|numeric|unique:comerciante'. ($id ? ",id,$id" : ''),
            	'idmercado'=>'required',
            	'fecha'=>'required',
                'idseccion'=>'required',
                'idcomerciante'=>'required',
                'valor'=>'required|numeric'
            ], $merge);
        }    
}
