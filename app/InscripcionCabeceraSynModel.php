<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscripcionCabeceraSynModel extends Model
{
    //
    protected $table = 'inscripcioncabecera';
    public  $timestamps = false;
    //protected $primaryKey = 'OID';
}
