<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MercadoModel extends Model
{
    //
     protected $table = 'mercado';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                                
                'descripcion'=>'required|unique:mercado'. ($id ? ",id,$id" : '')
            ], $merge);
        }   
}
