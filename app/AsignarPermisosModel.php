<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsignarPermisosModel extends Model
{
    //
    protected $table = 'ad_menuusuario';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'name'=>'required',
			'idmenumain'=>'required'
		], $merge);
    } 
}
