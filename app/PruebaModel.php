<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PruebaModel extends Model
{
    //
        protected $table = 'prueba';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'nombre'=>'required',
                'edad'=>'required|numeric'
            ], $merge);
        }   
}
