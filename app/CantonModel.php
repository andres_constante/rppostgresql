<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CantonModel extends Model
{
    //
    protected $table = 'Canton';
    /*
 "OID" serial NOT NULL,
  "DscaCanton" character varying(40),
  "IdProvincia" integer,
  "IsDefault" boolean,
  "OldCode" integer,
  "OptimisticLockField" integer,
  "GCRecord" integer,
    */
}
