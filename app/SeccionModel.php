<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeccionModel extends Model
{
    //
         protected $table = 'seccion';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                                
                'nombre'=>'required|unique:seccion'. ($id ? ",id,$id" : ''),
                'valor'=>'required|numeric'
            ], $merge);
        }   
}
