<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partes_InscrpModel extends Model
{
    //Partes_Inscrp
    protected $table = 'Partes_Inscrp';
    public $timestamps = false;
    protected $primaryKey = 'OID';
}
