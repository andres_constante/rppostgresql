<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibroModel extends Model
{
    //
    protected $table = 'Libro';
}
