<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscripcionModel extends Model
{
    //
    protected $table = 'Inscripcion';
    public $timestamps = false;
    protected $primaryKey = 'OID';
}
