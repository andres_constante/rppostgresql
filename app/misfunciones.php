<?php
function show($objeto,$i=1){
    echo "<pre>";
    print_r($objeto);
    if($i==1)
      die();
}
function ConfigSystem($clave="")
{
    $data=SysConfigModel::where("campo",$clave)->first();
    $res ='';
    if(!empty($data))
    {
        $res=$data->valor;
    }
    return $res;
}
    function Auditoria($accion)
    {
        $historial= new \App\AuditoriaModel();
        $historial->idusuario=Auth::user()->id;
        $historial->urlmenu= Request::getRequestUri();
        $historial->accion=$accion;
        $historial->ip=Request::getClientIp();
        //$historial->nompc= Request::getHost();
        $historial->nompc=gethostname();
        
        $historial->save();
    }  
/**
         * Devuelve una cadena de un número rellenado a la izquierda con un caracter. Ej. 10 => 000010.
         *
         * @param string $valor = Cadena a rellenar
         * @param int $longitud = Longitud de caracteres a rellenar
         * @param string $car = Caracter de relleno
         * @return string
         * @static 
         */
function Autorizar($url)
   {
       //die($url);
       $urlar=  explode("/", $url);
       $url=$urlar[0];
       if (Auth::user()->id!=1)
       {
            $tabla=DB::table("menu as a")
                ->join("ad_menuusuario as b","a.id","=","b.idmenu") 
                ->select("a.*")
                ->where(function($query) use ($url){
                    $query->where("a.ruta","like",$url."%")->where("b.idusuario",Auth::user()->id);
                })
                ->first();
            /*echo "<pre>";
            print_r($tabla);
            die();*/
            if(empty($tabla))  
            {
                Session::flash("msgacceso","Se detectó un acceso no permitido el ".  fechas(2). " a las ". date("H:i:s").".");
                
                Auditoria("Acceso no permitido 403");                
                die(Response::view('errors/403', array(), 403));
                
                
                //App::abort(403, 'Unauthorized action.');
                die("
                <html>
                    <head>
                        <meta charset='utf-8'>
                        <meta http-equiv='Refresh' content='5;url=".URL::to(URL::previous())."'>
                    </head>
                <body>
                    <p><h1>Acceso denegado</h1><h2>Será redireccionado automáticamente a la página prinipal. Caso contrario haga clic <a href='".URL::to(URL::previous())."'>aquí</a> para regresar</h2></p>
                </body>
                </html>"
                );                
            }
       }
   }
        /**
         * Devuelve una cadena de un número rellenado a la izquierda con un caracter. Ej. 10 => 000010.
         *
         * @param string $valor = Cadena a rellenar
         * @param int $longitud = Longitud de caracteres a rellenar
         * @param string $car = Caracter de relleno
         * @return string
         * @static 
         */
function CerosIzquierda($valor,$longitud=10,$car="0")
{
       return str_pad($valor, $longitud, $car, STR_PAD_LEFT);
}
        /**
         * Devuelve una cadena de un número rellenado a la derecha con un caracter. Ej. 10 => 000010.
         *
         * @param string $valor = Cadena a rellenar
         * @param int $longitud = Longitud de caracteres a rellenar
         * @param string $car = Caracter de relleno
         * @return string
         * @static 
         */

function CerosDerecha($valor,$longitud=10,$car="0")
{
       return str_pad($valor, $longitud, $car, STR_PAD_RIGHT);
}
        /**
         * Devuelve la fecha actual del sistema en formato date o letras
         *
         * @param int $a Tipo de Dato a devolver. (0) Fecha Hora Actual. (1) Solo fecha actual. (2) Fecha actual o fecha enviada en letras. (3) Fecha y hora actual
         * @param string $fecha SI no se desea fecha actual puede asignar una fecha personalizada. Ej. 2016-05-13
         * @return string Fecha: Ej. Viernes, 13 de mayo del 2016
         * @static 
         */
function fechas($a=0,$fecha='')
{        
        $mes=Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
        $dia=Array('Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','Sábado');        
        switch ($a)
        {
            case 0: // Solo fecha actual y hora
                $fecha=date("Y-m-d")." ".date("H:i:s");
                $fechar= $fecha;
                break;
 			case 1: // Solo fecha actual
                $fecha=date("Y-m-d");
                $fechar= $fecha;
                break;                
            case 2: // Devuelve la fecha en letras cuando se le envie una fecha cualquiera
                    if($fecha=='')
                        $fecha=date("Y-m-d")." ".date("H:i:s");
                    $mesn=date("n",strtotime($fecha));
                    $dian=date("w",strtotime($fecha));
                    $diad=date("d",strtotime($fecha));
                    $anio=date("Y",strtotime($fecha));
                    $fecha=$dia[$dian].", $diad de ".$mes[$mesn-1]." de $anio"; 
					$fechar= utf8_decode($fecha);
                break;                       
            case 3: // Devuelve la fecha en letras y la hora
                    if($fecha=='')
                        $fecha=date("Y-m-d")." ".date("H:i:s");
                    $mesn=date("n",strtotime($fecha));
                    $dian=date("w",strtotime($fecha));
                    $diad=date("d",strtotime($fecha));
                    $anio=date("Y",strtotime($fecha));
                    $hora=" a las ".date("H:i:s",strtotime($fecha));
                    $fecha=$dia[$dian].", $diad de ".$mes[$mesn-1]." de $anio $hora"; 
					$fechar= utf8_decode($fecha);
                break;
        }
        return $fechar;
}  
//
/**
         * Comprueba si un número de cédula ecuatoriana es correcta o no
         *
         * @param string $strCedula = Cédula
         * @return int (1) Correcto (0) Incorrecto         
         */
 function validarCI($strCedula)
    {
   $suma = 0;
   $strOriginal = $strCedula;
   $intProvincia = substr($strCedula,0,2);
   $intTercero = $strCedula[2];
   $intUltimo = $strCedula[9];
   if (! settype($strCedula,"float")) return FALSE;
   if ((int) $intProvincia < 1 || (int) $intProvincia > 25) return FALSE;
   //if ((int) $intTercero == 7 || (int) $intTercero == 4 )
           //return FALSE;
   for($indice = 0; $indice < 9; $indice++) {
     //echo $strOriginal[$indice],'</br>';
     switch($indice) {
        case 0:
        case 2:
        case 4:
        case 6:
        case 8:
           $arrProducto[$indice] = $strOriginal[$indice] * 2;
           if ($arrProducto[$indice] >= 10) $arrProducto[$indice] -= 9;
           //echo $arrProducto[$indice],'</br>';
           break;
        case 1:
        case 3:
        case 5:
        case 7:
           $arrProducto[$indice] = $strOriginal[$indice] * 1;
           if ($arrProducto[$indice] >= 10) $arrProducto[$indice] -= 9;
           //echo $arrProducto[$indice],'</br>';
           break;
     }
   }
   foreach($arrProducto as $indice => $producto) $suma += $producto;
   $residuo = $suma % 10;
   $intVerificador = $residuo==0 ? 0: 10 - $residuo;
   return ($intVerificador == $intUltimo? 1: 0);
}
/**
         * Limita el contenido de las palabras en una cadena de caracteres
         *
         * @param string $cadena Cadena a limitar
         * @param int $longitud Longitud de palabras a limitar
         * @return string Cadena limitada         
         */
function limitarPalabras($cadena, $longitud)
    {
        $palabras = explode(' ', $cadena);
        if (count($palabras) > $longitud)
            return implode(' ', array_slice($palabras, 0, $longitud));
        else
            return $cadena;
    }

?>