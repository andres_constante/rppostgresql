<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteModel extends Model
{
    //
    protected $table = 'Cliente';
    public  $timestamps = false;
    protected $primaryKey = 'OID';
}

