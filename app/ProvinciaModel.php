<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvinciaModel extends Model
{
    //
    protected $table="Provincia";
    /*
	"OID" serial NOT NULL,
  "DscaProvincia" character varying(40),
  "Sigla" character varying(3),
  "Region" integer,
  "IsDefault" boolean,
  "Bandera" bytea,
  "OldCode" integer,
  "OptimisticLockField" integer,
  "GCRecord" integer,
    */
}
