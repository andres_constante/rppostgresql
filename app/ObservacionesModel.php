<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObservacionesModel extends Model
{
    //Observaciones_Inscrp
    protected $table = 'Observaciones_Inscrp';
    public $timestamps = false;
    protected $primaryKey = 'OID';
}
