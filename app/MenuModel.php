<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    //
    protected $table = 'menu';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'menu'=>'required|unique:menu'. ($id ? ",id,$id" : ''),
                'ruta'=>'required',
                'nivel'=>'required',
                'idmain'=>'required',
                'orden'=>'required|numeric'
            ], $merge);
        }    
}
