<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscripcionDetalleModel extends Model
{
    //
    protected $table = 'inscripciondetalle';
    public  $timestamps = false;
}
