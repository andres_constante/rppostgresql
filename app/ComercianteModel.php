<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComercianteModel extends Model
{
    //
     protected $table = 'comerciante';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'nombre'=>'required',
                'cedula'=>'required|numeric|unique:comerciante'. ($id ? ",id,$id" : ''),
                'telefono'=>'required|numeric'
            ], $merge);
        }    
}
