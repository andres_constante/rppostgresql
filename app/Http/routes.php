<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get("pruebanom",function(){
	$nombrescli="MATTOS XXX LETTY MARIA";
    $nombrescli=str_replace(chr( 194 ) . chr( 160 ), " ",$nombrescli);
    //$nombrescli="CONSTANTE MURILLO ANDRES";
    //return $nombrescli;
	$nombrepar1=explode(' ',trim($nombrescli));
    return $nombrepar1;
                        $nombrecli="";
                        $apellidocli="";
                        $cono1=0;
                        foreach ($nombrepar1 as $key1 => $value1) {
                            if($cono1<2)
                                $apellidocli.=trim($value1). " ";
                            else
                                $nombrecli.=trim($value1). " ";
                            $cono1++;
                        }
                    $apellidocli=trim($apellidocli);
                    $nombrecli=trim($nombrecli);
     return array($apellidocli,$nombrecli);
});
Route::get("fechaprueba",function(){
	$fecha="1985/04/10";
	$fecha=date("Y-m-d",strtotime("10/04/1985"));
	echo $fecha;
});

Route::resource("depuracion","DepuracionController");
Route::get("reportemare","SincronizaController@reportemare");
Route::get("reporteingresos","SincronizaController@reporteingresos");
Route::post("reporteingresos","SincronizaController@reporteingresos");
Route::get("estadistica","SincronizaController@estadistica");
Route::post("estadistica","SincronizaController@estadistica");
Route::get("estadisticaresumen","SincronizaController@estadisticaresumen");
Route::post("estadisticaresumen","SincronizaController@estadisticaresumen");
Route::get("listarcabeceras","SincronizaController@listarcabeceras");
Route::get("listardetalles","SincronizaController@listardetalles");

Route::get("procesardatoscab","SincronizaController@procesardatoscab");
Route::get('procesardatosdeta',"SincronizaController@procesaintervinientes");


Route::resource('sincronizacionhome',"SincronizaController");
Route::resource('sincronizacionhomedeta',"SincronizaController@indexdeta");


//PRUEBAS
Route::get("pruebacabecera","SincronizaController@procesardatoscab");
Route::get('pruebaclientes',"SincronizaController@procesaintervinientes");


Route::get("pruebaguzzle","PruebaController@pruebaguzzle");
Route::get("pruebaconexion","PruebaController@pruebaconexion");


Route::get('/', function () {

    return view('home');
});
//Route::resource("pruebaregistro","PruebaController");
