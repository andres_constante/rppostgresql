<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\InscripcionCabeceraSynModel;
use App\InscripcionModel;
use App\LibroModel;
use App\ActosModel;
use App\CantonModel;
use App\Notaria_JuzgadoModel;
use App\LibroLibros_ActoActosModel;
use App\Partes_InscrpModel;
use App\InscripcionDetalleModel;
use App\ClienteModel;
use App\ObservacionesModel;
use App\TipoIdentificacionModel;
use App\EstadoCivilModel;
use App\CalidadCompareceModel;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Input;
use App\SincronizarDatosResulModel;
use App\InscripcionClientesProModel;
use App\CiudadModel;
use App\ProvinciaModel;
class SincronizaController extends Controller
{
    var $creadopor="eeb44ee6-899d-4fbb-a9be-523fcd5c79e1";//"a789e08f-cebf-47c9-a483-213a53925df7";//"8cde016d-9855-4b51-8f7a-44382e30fc20";//Carrillo 3adfd104-70d9-4d6e-95bb-6d5ec74c5afa
    public function showdata($objeto,$i=1){
    echo "<pre>";
    print_r($objeto);
    if($i==1)
      die();
    }
    public function estadisticaresumen()
    {
        $fecha1= date("Y-m-d");
        $fecha2= date("Y-m-d");
        $colapse="in";
        if(Input::has("desde")){
            $fecha1 = Input::get("desde");
            $fecha2 = Input::get("hasta");
            $colapse="";
        }
        $procesados=SincronizarDatosResulModel::join("Inscripcion as a","a.OID","=","sincronizardatosresul.idmain")
        ->join("Libro as b","b.OID","=","a.IdLibro")
        ->select(DB::raw("to_char(a.\"FechaInscr\", 'YYYY') as anio"),"b.DscaLibro as tipo",DB::raw("count(*) as total"))        
        ->groupBy(DB::raw("to_char(a.\"FechaInscr\", 'YYYY')"))
        ->groupBy("b.DscaLibro")        
        ->orderby(DB::raw("to_char(a.\"FechaInscr\", 'YYYY')"))
        ->orderby("b.DscaLibro")        
        ->whereBetween("sincronizardatosresul.created_at",array("$fecha1 00:00:00","$fecha2 23:59:59"))
        ->get();
        //return $procesados->toarray();
        return view('vistas.resulestadisticaresumen',["procesados"=>$procesados,"fecha1"=>$fecha1,"fecha2"=>$fecha2,"colapse"=>$colapse]);   
    }
    public function reemplazaespeciales($string,$especiales="SI",$tildes="SI",$espacio="NO")
    {

        $string = trim($string);

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );
        if($tildes!="SI")
            return $string;

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );
        if($especiales=="SI"){
          //Esta parte se encarga de eliminar cualquier caracter extraño
          $string = str_replace(
              array("\\", "¨", "º", "-", "~",
                   "#", "@", "|", "!", "\"",
                   "·", "$", "%", "&", "/",
                   "(", ")", "?", "'", "¡",
                   "¿", "[", "^", "`", "]",
                   "+", "}", "{", "¨", "´",
                   ">", "< ", ";", ",", ":",
                   " "),
              '',
              $string
          );
      }
        return $string;
    } 
    public function reportemare(){
        $tabla=InscripcionModel::join("Partes_Inscrp as a","a.IdInscripcion","=","Inscripcion.OID")
        ->join("Libro as b","b.OID","=","Inscripcion.IdLibro")
        ->select("b.DscaLibro",DB::raw("to_char(a.\"FechaIngreso\", 'MM') as Mes"),DB::raw("count(*) as total"))
        ->where(DB::raw("to_char(a.\"FechaIngreso\", 'YYYY')"),'2018')
        ->whereIn(DB::raw("to_char(a.\"FechaIngreso\", 'MM')"),array('04','05','06'))
        ->groupBy("b.DscaLibro")
        ->groupBy(DB::raw("to_char(a.\"FechaIngreso\", 'MM')"))
        ->orderby(DB::raw("to_char(a.\"FechaIngreso\", 'MM')"))
        ->orderby("b.DscaLibro")
        ->get();
        //$tabla=$tabla->count();
        return $tabla;
    }
    public function reporteingresos()
    {
        $fecha1= date("Y-m-d");
        $fecha2= date("Y-m-d");
        $colapse="in";
        if(Input::has("desde")){
            $fecha1 = Input::get("desde");
            $fecha2 = Input::get("hasta");
            $colapse="";
        }
        $procesados=SincronizarDatosResulModel::join("Inscripcion as a","a.OID","=","sincronizardatosresul.idmain")
        ->join("Libro as b","b.OID","=","a.IdLibro")
        ->join("Acto as c","c.OID","=","a.IdActo")
        ->select("a.*",DB::raw("to_char(sincronizardatosresul.created_at, 'YYYY-MM-DD') as fecha"),"b.DscaLibro","c.DescActo")
        ->whereBetween("sincronizardatosresul.created_at",array("$fecha1 00:00:00","$fecha2 23:59:59"))
        ->get();
        //show($procesados->toarray());
        return view('vistas.datosinscripcion',["procesados"=>$procesados,"fecha1"=>$fecha1,"fecha2"=>$fecha2,"colapse"=>$colapse]);   
    }
    public function estadistica()
    {
        $fecha1= date("Y-m-d");
        $fecha2= date("Y-m-d");
        $colapse="in";
        if(Input::has("desde")){
            $fecha1 = Input::get("desde");
            $fecha2 = Input::get("hasta");
            $colapse="";
        }
        $procesados=SincronizarDatosResulModel::join("Inscripcion as a","a.OID","=","sincronizardatosresul.idmain")
        ->join("Libro as b","b.OID","=","a.IdLibro")
        ->select(DB::raw("to_char(sincronizardatosresul.created_at, 'YYYY-MM-DD') as fecha"),"b.DscaLibro as tipo",DB::raw("to_char(a.\"FechaInscr\", 'YYYY') as anio"),DB::raw("count(*) as total"))
        ->groupBy(DB::raw("to_char(sincronizardatosresul.created_at, 'YYYY-MM-DD')"))
        ->groupBy("b.DscaLibro")
        ->groupBy(DB::raw("to_char(a.\"FechaInscr\", 'YYYY')"))
        ->orderby(DB::raw("to_char(sincronizardatosresul.created_at, 'YYYY-MM-DD')"))
        ->orderby("b.DscaLibro")
        ->orderby(DB::raw("to_char(a.\"FechaInscr\", 'YYYY')"))
        ->whereBetween("sincronizardatosresul.created_at",array("$fecha1 00:00:00","$fecha2 23:59:59"))
        ->get();

        return view('vistas.resulestadistica',["procesados"=>$procesados,"fecha1"=>$fecha1,"fecha2"=>$fecha2,"colapse"=>$colapse]);   
    }
    public function savehistorial($idmain,$observa="PROCESADO")
    {
        $sw=0;
        try{
            DB::beginTransaction();
            $save= new SincronizarDatosResulModel;
            $save->idmain=$idmain;
            $save->creadopor=$this->creadopor;
            $save->observacion=$observa;
            $save->save();
            $sw=1;
            DB::commit();        
        }catch(\Exception $e){
            // ROLLBACK
            DB::rollback();            
        }        
        return $sw;
    }
    public function listardetalles()
    {
        $procesados=InscripcionDetalleModel::orderby("id")->get();
        return view('vistas.datosdeta',["procesados"=>$procesados]);   
    }
    public function listarcabeceras()
    {
        $procesados=InscripcionCabeceraSynModel::orderby("id")->get();
        return view('vistas.datoscab',["procesados"=>$procesados]);
    }
    public function procesocliente($cliente)
    {
        DB::beginTransaction();
        try{
            $save=new InscripcionClientesProModel;
            $save->idcliente=$cliente["idcliente"];
            $save->observacion=$cliente["observacion"];
            $save->estado=$cliente["st"];
            $save->save();
            DB::commit();        
        }catch(\Exception $e){
            // ROLLBACK
            DB::rollback();
            //$mensaje[]=array("Error"=>$e->getMessage());
            //$intervinientes=array("idcliente"=>0,"tipocliente"=>0,"stcivil"=>0,"observacion"=>$e->getMessage()." ".$e->getLine());
        }
    }
    public function insertacliente($datoscliente,$obs="",$sw=0)
    {
        //$datoscliente=array("tipocliente"=>trim($valuede->tipo_cliente),"cedruc"=>"0000000000","nombres"=>$nombrecli,"apellidos"=>$apellidocli,"stcivil"=>trim($valuede->estado_civil),"tipoidentificacion"=>"NINGUNO");        
        try{
            
            //Tipo Cliente. Natural/Juridico
            $tipocli=$this->reemplazaespeciales($datoscliente["tipocliente"],"SI","NO");
            if($tipocli=="NATURAL")
                $tipocli=1;
            else
                $tipocli=2;
            //Estado Civil
            $stcivil=trim($this->reemplazaespeciales($datoscliente["stcivil"],"SI","NO"));
            if($stcivil=="CASADA")
                $stcivil="CASADO";
            $stcivilst= EstadoCivilModel::where("DscaEstadoCivil","like","%$stcivil%")->first();
            if(empty($stcivilst))
                $stcivil=6;
            else
                $stcivil=$stcivilst->OID;
            //Tipo Identificacion
            $tipoidentb=TipoIdentificacionModel::where("Descripcion","like","%".trim($datoscliente["tipoidentificacion"]."%"))->first();
            if(empty($tipoidentb))
                $tipoiden=0;
            else
                $tipoiden=$tipoidentb->OID;
            //
            //Tercera Edad
            $ter=strtotime($datoscliente["fecha_naci"]);
            $ter=intval(date("Y")) - intval(date("Y",$ter));
            $tercera=false;
            //$this->showdata($tipocli);
            if($ter>64)
                $tercera=true;
            $datoscliente["apellidos"] = str_replace(
              array("\\", "¨", "º", "-", "~",
                   "#", "@", "|", "!", "\"",
                   "·", "$", "%", "&", "/",
                   "(", ")", "?", "'", "¡",
                   "¿", "[", "^", "`", "]",
                   "+", "}", "{", "¨", "´",
                   ">", "< ", ";", ",", ":","“","”"
                   ),
              '',
              $datoscliente["apellidos"] 
            );
            $datoscliente["nombres"] = str_replace(
              array("\\", "¨", "º", "-", "~",
                   "#", "@", "|", "!", "\"",
                   "·", "$", "%", "&", "/",
                   "(", ")", "?", "'", "¡",
                   "¿", "[", "^", "`", "]",
                   "+", "}", "{", "¨", "´",
                   ">", "< ", ";", ",", ":","“","”"
                   ),
              '',
              $datoscliente["nombres"] 
            );
            $ran=trim($datoscliente["cedruc"]) ;
            $datoscliente["cedruc"]=(trim($datoscliente["cedruc"])=="" ? strtoupper(str_random(8)) : trim($datoscliente["cedruc"])) ;            
            //$this->showdata($tercera);
            //$this->showdata($datoscliente);
            //return array("idcliente"=>0,"tipocliente"=>0,"stcivil"=>0,"observacion"=>$e->getMessage()." ".$e->getLine());
            DB::beginTransaction();
            $guardar=new ClienteModel;
            $guardar->TipoCliente=$tipocli;
            $guardar->CedRuc=trim($datoscliente["cedruc"]);
            if($tipocli==1){
                $guardar->Apellidos=trim(strtoupper(trim($datoscliente["apellidos"])));
                $guardar->Nombres=trim(strtoupper(trim($datoscliente["nombres"])));
            }else
            {
                $guardar->NombreEmpresa=strtoupper(trim(trim($datoscliente["apellidos"]) ." ".trim($datoscliente["nombres"])));
                $stcivil=6;
            }
            if(array_key_exists("razon",$datoscliente))
            {
                $guardar->NombreEmpresa=$datoscliente["razon"];
            }
            $guardar->StCivil=$stcivil;
            $guardar->TipoIdentificacion=$tipoiden;
            /*Localidad*/
            $guardar->IdProvincia= $datoscliente["provincia"];
            $guardar->IdCanton= $datoscliente["canton"];
            $guardar->IdCiudad= $datoscliente["ciudad"];
            /**/
            $guardar->CreadoPor=$this->creadopor;
            $guardar->FechaIngreso=date("Y-m-d H:i:s");
            $guardar->FechaNacimiento=$datoscliente["fecha_naci"];
            $guardar->TerceraEdad=$tercera;
            $guardar->save();
            $id=$guardar->OID;
            $guardar=ClienteModel::find($id);
            if($datoscliente["tipoidentificacion"]=="NINGUNO" && $ran=="")
            {
                $guardar->CedRuc="CLTE$id";
            }
            if($ran=="")
            {
                $guardar->CedRuc="CLTE$id";
            }
            $guardar->Codigo="CLTE$id";
            $guardar->save();            
            $intervinientes=array("idcliente"=>$id,"tipocliente"=>$tipocli,"stcivil"=>$stcivil,"observacion"=>"Procesado Exitosamente");
            //DB::rollback();
            DB::commit(); 
            $this->procesocliente(array("idcliente"=>$guardar->OID,"observacion"=>$obs,"st"=>$sw));
        }catch(\Exception $e){
            // ROLLBACK
            DB::rollback();
            //$mensaje[]=array("Error"=>$e->getMessage());
            $intervinientes=array("idcliente"=>0,"tipocliente"=>0,"stcivil"=>0,"observacion"=>$e->getMessage()." ".$e->getLine());
        }
        //$this->showdata($intervinientes);
        return $intervinientes;
    }
    public function procesaintervinientes()
    {
        //$detalle=InscripcionDetalleModel::all();        
        $tot=100;
        if(Input::has("total"))
        {
            $tot=intval(Input::get("total"));
        }
        //$detalle=InscripcionDetalleModel::take($tot)->get();
        $detalle=InscripcionDetalleModel::where("estado","PENDIENTE")->where("idcab","<>",0)->take($tot);
        $totalpendiente=InscripcionDetalleModel::where("estado","PENDIENTE")->where("idcab","<>",0)->count();
        $totalproce=InscripcionDetalleModel::where("estado","<>","PENDIENTE")->where("idcab","<>",0)->count();
        $totalmain=$totalpendiente+$totalproce;                
        if($detalle->count()==0)
            return 1;
        $porcentaje=($totalproce/$totalmain)*100;
        $porcentaje=intval($porcentaje);
        //return $detalle->count();
        $detalle=$detalle->get();
        $resulbus=array();
        $prosi=0;        
        $prono=0;
        $intervinientesproce=array();
        //$this->showdata($detalle->toarray());
        foreach ($detalle as $keyde => $valuede) 
        {                        
            if(trim($valuede->calidad_comparece) =="" && trim($valuede->nombre_razon)=="")                
            {
                $intervinientes=array("idcliente"=>0,"tipocliente"=>"","stcivil"=>"","papel"=>trim($valuede->calidad_comparece),"fullname"=>$valuede->nombre_razon,"observacion"=>"En blanco","iddeta"=>$valuede->id);
                $this->actualizarestadodeta($valuede->id,"NO PROCESADO","Campos en blanco");
                /*Actualizar Estado de Proceso*/
                $intervinientesproce[]= $intervinientes;
                continue;
            }                            
            if($valuede->idcab==0)
            {
                $intervinientes=array("idcliente"=>0,"tipocliente"=>$valuede->tipo_cliente,"stcivil"=>$valuede->estado_civil,"papel"=>trim($valuede->calidad_comparece),"fullname"=>$valuede->nombre_razon,"observacion"=>"No existe Inscripción para este registro","iddeta"=>$valuede->id);
                $this->actualizarestadodeta($valuede->id,"NO PROCESADO","No existe Inscripción para este registro");
                /*Actualizar Estado de Proceso*/
                $intervinientesproce[]= $intervinientes;        
                continue;
            }
            $cedula=trim($this->reemplazaespeciales($valuede->cedula));              
            //$this->showdata($cedula);
            $busnom=0;
            if ($cedula=="")
            {
                $busnom=1;
            }elseif(!is_numeric($cedula))
            {
                $busnom=1;
            }elseif(strlen($cedula)==10 || strlen($cedula)==13){
                $busnom=0;
            }else{
                $busnom=1;
            }            
            //echo $cedula."-";             
            $nombre_razon=trim($valuede->nombre_razon);
            $idcliente=0;
            $nombrepar=explode(" ",$nombre_razon);                
                $nombre_razon="";
                $nombret="";
                $cono=0;
                $nombrecli="";
                $apellidocli="";
                foreach ($nombrepar as $key => $value) {
                    if(trim($value)!="")
                        $nombre_razon.=trim($value)." ";
                    if($cono<3)
                    {
                        $nombret.=trim($value);
                    }
                    if($cono<2)
                        $apellidocli.=trim($value);
                    else
                        $nombrecli.=trim($value);
                    $cono++;
                }
            //$this->showdata(array("busnom"=>$busnom));
            if($busnom==1)
            {
                //echo $nombre_razon;
                
                //return $nombre_razon;
                //echo $nombre_razon. "<br>";
                $nombre_razon=str_replace(" ","",trim($nombre_razon));
                $nombret=$this->reemplazaespeciales($nombret,"SI","NO");                
                /*
                $cliente=ClienteModel::where(DB::raw("concat(\"Apellidos\",' ',\"Nombres\")"),"like","%".$nombre_razon."%")
                ->orWhere(DB::raw("concat(\"Nombres\",' ',\"Apellidos\")"),"like","%".$nombre_razon."%")
                */
                $nombre_razon= str_replace(
              array("\\", "¨", "º", "-", "~",
                   "#", "@", "|", "!", "\"",
                   "·", "$", "%", "&", "/",
                   "(", ")", "?", "'", "¡",
                   "¿", "[", "^", "`", "]",
                   "+", "}", "{", "¨", "´",
                   ">", "< ", ";", ",", ":","“","”"
                   ),
              '',
              $nombre_razon 
            );
                $cliente=ClienteModel::where(DB::raw("concat(replace(\"Apellidos\",' ',''),replace(\"Nombres\",' ',''))"),"like","%".$nombre_razon."%")
                ->orWhere(DB::raw("concat(replace(\"Nombres\",' ',''),replace(\"Apellidos\",' ',''))"),"like","%".$nombre_razon."%")
                ->orWhere(DB::raw("concat(replace(\"Apellidos\",' ',''),replace(\"Nombres\",' ',''))"),"like","%".$nombret."%")
                ->orWhere(DB::raw("concat(replace(\"Nombres\",' ',''),replace(\"Apellidos\",' ',''))"),"like","%".$nombret."%")
                ->orWhere(DB::raw("replace(\"NombreEmpresa\",' ','')"),"like","%".$nombret."%")
                ->orWhere(DB::raw("replace(\"NombreEmpresa\",' ','')"),"like","%".$nombre_razon."%")
                ->first();
                //  $this->showdata($cliente);
                if(empty($cliente)){
                    $idcliente=-1;  
                    $prono++;
                    $resulbus[]=array("busnom"=>$busnom,"cedula"=>$cedula,"Razon"=>$nombre_razon,"ID_Cliente"=>$idcliente);
                    /*Insertar*/
                    $nombresclichar=str_replace(chr( 194 ) . chr( 160 ), " ",$valuede->nombre_razon);
                    $nombrepar1=explode(" ",trim($nombresclichar));                                
                        $nombrecli="";
                        $apellidocli="";
                        $cono1=0;
                        foreach ($nombrepar1 as $key1 => $value1) {
                            if($cono1<2)
                                $apellidocli.=trim($value1). " ";
                            else
                                $nombrecli.=trim($value1). " ";
                            $cono1++;
                        }
                    $apellidocli=trim($apellidocli);
                    $nombrecli=trim($nombrecli);

                    $datoscliente=array("tipocliente"=>trim($valuede->tipo_cliente),"cedruc"=>"","nombres"=>$nombrecli,"apellidos"=>$apellidocli,"stcivil"=>trim($valuede->estado_civil),"tipoidentificacion"=>"NINGUNO","provincia"=>11,"canton"=>44,"ciudad"=>43,"fecha_naci"=>null);
                    //$this->showdata($datoscliente);
                    $clientedatos=$this->insertacliente($datoscliente,"No encontrado en Base");
                    $intervinientes=array("idcliente"=>$clientedatos["idcliente"],"tipocliente"=>$clientedatos["tipocliente"],"stcivil"=>$clientedatos["stcivil"],"papel"=>trim($valuede->calidad_comparece),"fullname"=>$apellidocli." ".$nombrecli,"observacion"=>$clientedatos["observacion"],"iddeta"=>$valuede->id);
                }
                else{
                    $idcliente=$cliente->OID;
                    $resulbus[]=array("busnom"=>$busnom,"cedula"=>$cedula,"Razon"=>$nombre_razon,"ID_Cliente"=>$idcliente);
                    $prosi++;
                    $intervinientes=array("idcliente"=>$idcliente,"tipocliente"=>$cliente->TipoCliente,"stcivil"=>$cliente->StCivil,"papel"=>trim($valuede->calidad_comparece),"fullname"=>$cliente->Apellidos." ".$cliente->Nombres,"observacion"=>"Cliente Encontrado por Nombre","iddeta"=>$valuede->id);
                }
            }else{                
                $cliente=ClienteModel::where(Db::raw("replace(trim(\"CedRuc\"),'-','')"),"like",$cedula)->first();
                //$this->showdata($cliente);
                if(empty($cliente)){
                    $client = new GuzzleHttpClient();
                    //$urlru="http://webserver.manta.gob.ec:9083/api/Deudas/Obtener_deuda_general/1309366514";
                    $sw=0;
                    $obs="";
                    $stds=1;
                    if(strlen($cedula)==10)
                    {
                        $urlru="http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/$cedula/1015";
                    }else{
                        $urlru="http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/$cedula/1021";
                        $sw=1;                        
                    }
                    try {
                        $respuesta = $client->request('GET',$urlru);
                        $clienteds= json_decode($respuesta->getBody()->getContents(),true);                        
                    } catch (\Exception $e) {
                        //show(array("error",$e->getMessage()));
                        //$resulbus[]=array("Error: ".$e->getMessage());
                        $sw=-1;
                        $clienteds[0]=array($e->getMessage()." ".$e->getLine());                        
                    }
                    $clienteds=$clienteds[0];
                    //$this->showdata($clienteds);
                    if(count($clienteds)<=1)
                        $sw=-1;                    
                    if($sw<0)
                    {
                        $obs="No se encuentra en Dato Seguro";                           
                        $nombrepar1=explode(" ",$nombre_razon);                                
                        $nombrecli="";
                        $apellidocli="";
                        $cono1=0;
                        foreach ($nombrepar1 as $key1 => $value1) {                            
                            if($cono1<2)
                                $apellidocli.=trim($value1). " ";
                            else
                                $nombrecli.=trim($value1). " ";
                            $cono1++;
                        }
                        $apellidocli=trim($apellidocli);
                         $nombrecli=trim($nombrecli);
                        $datoscliente=array("tipocliente"=>trim($valuede->tipo_cliente),"cedruc"=>$cedula,"nombres"=>$nombrecli,"apellidos"=>$apellidocli,"stcivil"=>trim($valuede->estado_civil),"tipoidentificacion"=>"NINGUNO","provincia"=>11,"canton"=>44,"ciudad"=>43,"fecha_naci"=>null);
                        
                    }elseif($sw==0)
                    {
                        $obs="Dato Seguro por Cédula";
                        $stds=2;
                        $nombrepar1=explode(" ",$clienteds["nombre"]);                                
                        $nombrecli="";
                        $apellidocli="";
                        $cono1=0;
                        foreach ($nombrepar1 as $key1 => $value1) {                            
                            if($cono1<2)
                                $apellidocli.=trim($value1). " ";
                            else
                                $nombrecli.=trim($value1). " ";
                            $cono1++;
                        }
                        $apellidocli=trim($apellidocli);
                         $nombrecli=trim($nombrecli);
                         /*Provincia de Nacimiento*/
                           //$this->showdata($clienteds);
                           $lugar=trim($clienteds["lugarNacimiento"]);
                           $lugar=explode("/",$lugar);
                           //$this->showdata($lugar);
                           if(count($lugar)<=1)
                           {
                                $provincia=11;
                                $canton=44;
                                $ciudad=43;
                           }else{
                           $provincia=ProvinciaModel::where("DscaProvincia","like","%".$lugar[0]."%")->first();
                           //$this->showdata($provincia);
                           if(empty($provincia))
                                $provincia=11;
                            else
                                $provincia=$provincia->OID;
                            //////
                            $canton=CantonModel::where("DscaCanton","like","%".$lugar[1]."%")->first();
                            //$this->showdata($canton);
                           if(empty($canton))
                                $canton=44;
                            else
                                $canton=$canton->OID;
                            //////
                            $ciudad=CiudadModel::where("DscaCiudad","like","%".$lugar[2]."%")->first();
                            //$this->showdata($ciudad);
                           if(empty($ciudad))
                                $ciudad=43;
                            else
                                $ciudad=$ciudad->OID;
                            }
                            //$this->showdata(array($provincia,$canton,$ciudad));
                         /**/
                        $datoscliente=array("tipocliente"=>trim($valuede->tipo_cliente),"cedruc"=>$clienteds["cedula"],"nombres"=>$nombrecli,"apellidos"=>$apellidocli,"stcivil"=>trim($clienteds["estadoCivil"]),"tipoidentificacion"=>"CEDULA","provincia"=>$provincia,"canton"=>$canton,"ciudad"=>$ciudad,"fecha_naci"=>date("Y-m-d",strtotime(str_replace('/','-',$clienteds["fechaNacimiento"]))));
                    }else{
                        $obs="Busqueda Dato Seguro por RUC";
                        $stds=3;
                        $nombrepar1=explode(" ",$clienteds["razonSocial"]);                                
                        $nombrecli="";
                        $apellidocli="";
                        $cono1=0;
                        foreach ($nombrepar1 as $key1 => $value1) {                            
                            if($cono1<2)
                                $apellidocli.=trim($value1). " ";
                            else
                                $nombrecli.=trim($value1)." ";
                            $cono1++;
                        }
                        $apellidocli=trim($apellidocli);
                         $nombrecli=trim($nombrecli);
                        $datoscliente=array("tipocliente"=>trim($valuede->tipo_cliente),"cedruc"=>$clienteds["numeroRuc"],"nombres"=>$nombrecli,"apellidos"=>$apellidocli,"stcivil"=>"SOLTERO","tipoidentificacion"=>"RUC","provincia"=>11,"canton"=>44,"ciudad"=>43,"fecha_naci"=>null,"razon"=>$nombre_razon);
                    }
                    //$this->showdata($datoscliente);
                    $clientedatos=$this->insertacliente($datoscliente,$obs,$stds);
                    $intervinientes=array("idcliente"=>$clientedatos["idcliente"],"tipocliente"=>$clientedatos["tipocliente"],"stcivil"=>$clientedatos["stcivil"],"papel"=>trim($valuede->calidad_comparece),"fullname"=>$apellidocli." ".$nombrecli,"observacion"=>"Dato Seguro $sw: ".$clientedatos["observacion"],"iddeta"=>$valuede->id);
                }
                else{
                    $idcliente=$cliente->OID;
                    $resulbus[]=array("busnom"=>$busnom,"cedula"=>$cedula,"Razon"=>$nombre_razon,"ID_Cliente"=>$idcliente);                    
                    $intervinientes=array("idcliente"=>$idcliente,"tipocliente"=>$cliente->TipoCliente,"stcivil"=>$cliente->StCivil,"papel"=>trim($valuede->calidad_comparece),"fullname"=>$cliente->Apellidos." ".$cliente->Nombres,"observacion"=>"Cliente encontrado por Cedula","iddeta"=>$valuede->id);
                    $prosi++;
                }   
            }            
            /*Insertar Intervinientes*/
            $errin="";
            $idmain=0;
            $estadopro="NO PROCESADO";            
            if(intval($intervinientes["idcliente"])!=0)
            {
                $existede=Partes_InscrpModel::where("IdInscripcion",$valuede->idcab)->where("IdCliente",$intervinientes["idcliente"])->first();
                if(empty($existede))
                {                
                try{                    
                    DB::beginTransaction();
                    $intersave=new Partes_InscrpModel;

                    $intersave->IdInscripcion=$valuede->idcab;
                    $intersave->FechaIngreso=date("Y-m-d H:i:s");
                    $intersave->IdCliente=$intervinientes["idcliente"];
                    $intersave->TipoCliente=$intervinientes["tipocliente"];
                    $intersave->StCivil=$intervinientes["stcivil"];
                    $calidad=CalidadCompareceModel::where("Calidad","like","%".$intervinientes["papel"]."%")->first();
                    $idpapel=80;
                    if(!empty($calidad))
                    {
                        $idpapel=$calidad->OID;
                    }
                    $intersave->CalidadComparece=$idpapel;
                    $intersave->IdUserCrea=$this->creadopor;
                    $intersave->FullName=$valuede->nombre_razon;
                    /**/
                    $intersave->OptimisticLockField=1;
                    $intersave->EsMenorDeEdad=false;
                    $intersave->save();
                    $idmain=$intersave->OID;
                    $estadopro="PROCESADO";
                    //DB::rollback();
                    DB::commit();
                }catch(\Exception $e){
                    // ROLLBACK
                    DB::rollback();
                    $errin=". ".$e->getMessage(). " ".$e->getLine();
                    $idmain=-3;
                }
                }else{
                 $errin=". Ya existe el registro de Interviniente";
                 $idmain=-2;
                }
            }//END if
            else
            {
               $errin=". Cliente no válido."; 
               $idmain=-1;
            }
            $intervinientes["observacion"]=$intervinientes["observacion"].$errin;
            
            $this->actualizarestadodeta($valuede->id,$estadopro,$intervinientes["observacion"],$idmain);
            /*Actualizar Estado de Proceso*/
            $intervinientesproce[]= $intervinientes;        
        }//END For
        //return $intervinientesproce;
        return view('vistas.resuldeta',["procesados"=>$intervinientesproce,"totalpendiente"=>$totalpendiente,"totalproce"=>$totalproce,"porcentaje"=>$porcentaje,"totalmain"=>$totalmain]);
    }
    public function actualizarestadodeta($id,$estado="PROCESADO",$obs="Procesado exitosamente",$st_estado=0)
    {
        /*=========================================================*/
        try{
            DB::beginTransaction();
            $guardar=InscripcionDetalleModel::find($id);
            $guardar->estado=$estado;
            $guardar->detalleproceso=$obs;
            $guardar->fechaproceso=date("Y-m-d H:i:s");  
            $guardar->st_estado=$st_estado;          
            $guardar->save();
            //DB::rollback();
            DB::commit();        
        }catch(\Exception $e){
            // ROLLBACK
            DB::rollback();
            $procesados[]=array("Error"=>$e->getMessage(). " ".$e->getLine(),"observa"=>$obs,"st_estado"=>$st_estado);
        }
    }
    public function actualizarestadocab($idproceso,$id,$estado="PROCESADO",$obs="Procesado exitosamente",$st_estado=0)
    {
        /*=========================================================*/
        try{
            DB::beginTransaction();
            $guardar=InscripcionCabeceraSynModel::find($id);
            $guardar->estado=$estado;
            $guardar->detalleproceso=$obs;
            $guardar->fechaproceso=date("Y-m-d H:i:s");
            $guardar->idproceso=$idproceso;
            $guardar->st_estado=$st_estado;
            $guardar->save();
            //DB::rollback();            
            DB::commit();        
        }catch(\Exception $e){
            // ROLLBACK
            DB::rollback();
            //$procesados[]=array("Error"=>$e->getMessage(). " ".$e->getLine(),"observa"=>$value->id_inscripcion,"id_main"=>$idmain);
        }
    }
    public function procesardatoscab()
    {
        $tot=400;
        if(Input::has("total"))
        {
            $tot=intval(Input::get("total"));
        }
        //
        /*phpinfo();
        die();
        */
        //$tabla= InscripcionCabeceraSynModel::all();
        $tabla= InscripcionCabeceraSynModel::where("estado","PENDIENTE")->take($tot);
        $totalpendiente=InscripcionCabeceraSynModel::where("estado","PENDIENTE")->count();
        $totalproce=InscripcionCabeceraSynModel::where("estado","<>","PENDIENTE")->count();
        $totalmain=$totalpendiente+$totalproce;
        $porcentaje=($totalproce/$totalmain)*100;
        $porcentaje=intval($porcentaje);        
        //return $tabla->count();
        if($tabla->count()==0)
            return 1;
        $tabla=$tabla->orderby("id")->get();
        //$tabla= InscripcionCabeceraSynModel::whereIn("id",[44884,44885])->get();        
        //return $tabla;        
        $mensaje=array();
        $procesados=array();
        //$this->showdata($tabla);
            # code...
            try{
                DB::beginTransaction();
                foreach ($tabla as $key => $value) {
                    /*
                    $datos=array();
                    $datos["fecha_ingreso"]= strtotime(str_replace("/","-",$value->fecha_ingreso));
                    return $datos;
                    */
                    $libro=LibroModel::where("DscaLibro","like",trim($value->libro)."%")
                        ->whereNotNull("IdTpLibro")->first();//->DscaLibro;
                    //$this->showdata($libro->toarray());    
                    if(!$libro){
                        $mensaje[]=array("Cabecera"=>"ID: ".$value->id. " - ID_Inscripcion: ".$value->id_inscripcion. " LIBRO NO PROCESADO");
                        $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$value->id_inscripcion. " LIBRO NO PROCESADO","idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
                        #Actualizar Estado de Propceso
                        $this->actualizarestadocab(0,$value->id,"NO PROCESADO","LIBRO NO ENCONTRADO: ".$value->libro);
                        continue;
                    }
                    //
                    $actitos=explode("-",trim($value->acto_contrato));
                    if(count($actitos))
                    {
                        if(intval($actitos[0])!=0)
                            $actosbus=LibroLibros_ActoActosModel::where("OID",$actitos[0])->first();
                        else
                            $actosbus=array();
                        //return $actosbus;
                        if(empty($actosbus)){
                            $mensaje[]=array("Cabecera"=>"ID: ".$value->id. " - ID_Inscripcion: ".$value->id_inscripcion. " ACTO NO PROCESADO");
                            $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$value->id_inscripcion. " ACTO NO PROCESADO","idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
                            $this->actualizarestadocab(0,$value->id,"NO PROCESADO","ACTO NO ENCONTRADO: ".$actitos[0]);
                            continue;
                        }
                    }else{
                        $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$value->id_inscripcion. " ACTO NO PROCESADO","idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
                            $this->actualizarestadocab(0,$value->id,"NO PROCESADO","ACTO NO ENCONTRADO: ".$actitos[0]);
                            continue;
                    }                    
                    //return $actosbus;
                    //$acto=ActosModel::where("DescActo",$value->acto_contrato)->first();
                    $canton1str=str_replace(" ","",trim($value->canton));
                    $canton1=CantonModel::where(DB::raw("replace(\"DscaCanton\",' ','')"),"like",$canton1str."%")->first();
                    if(!$canton1){
                        $mensaje[]=array("Cabecera"=>"ID: ".$value->id. " - ID_Inscripcion: ".$value->id_inscripcion. " CANTON NO PROCESADO");
                        $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$value->id_inscripcion. " CANTON NO PROCESADO","idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
                        $this->actualizarestadocab(0,$value->id,"NO PROCESADO","CANTON NO ENCONTRADO: ".$value->canton);
                        continue;
                    }                    
                    $notar=$this->reemplazaespeciales(trim($value->notaria_juzgado),"SI","SI");
                    $notar1=str_replace(" ","",trim($value->notaria_juzgado));
                    //show($notar);
                    //$notaria=Notaria_JuzgadoModel::where("DscaEntidad","like","%".trim($value->notaria_juzgado)."%")->first();
                    $notaria=Notaria_JuzgadoModel::where(DB::raw("replace(\"DscaEntidad\",' ','')"),"like","%".$notar."%")
                    ->orWhere(DB::raw("replace(\"DscaEntidad\",' ','')"),"like","%".$notar1."%")->first();
                    if(!$notaria){
                        $mensaje[]=array("Cabecera"=>"ID: ".$value->id. " - ID_Inscripcion: ".$value->id_inscripcion. " NOTARIA NO PROCESADO");
                        $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$value->id_inscripcion. " NOTARIA NO PROCESADO","idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
                        $this->actualizarestadocab(0,$value->id,"NO PROCESADO","NOTARIA NO ENCONTRADO: ".$value->notaria_juzgado);
                        continue;
                    }
                    //$canton2=CantonModel::where(DB::raw("replace(\"DscaCanton\",' ','')"),"like","%".trim($value->canton_notaria)."%")->first();  
                    $canton2str=str_replace(" ","",trim($value->canton_notaria)); 
                    $canton2=CantonModel::where(DB::raw("replace(\"DscaCanton\",' ','')"),"like",$canton2str."%")->first();
                    //return  $canton2;
                    if(!$canton2){
                        $mensaje[]=array("Cabecera"=>"ID: ".$value->id. " - ID_Inscripcion: ".$value->id_inscripcion. " CANTON NOTARIA NO PROCESADO");
                        $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$value->id_inscripcion. " CANTON NOTARIA NO PROCESADO","idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
                        $this->actualizarestadocab(0,$value->id,"NO PROCESADO","CANTON NOTARIA NO ENCONTRADO: ".$value->canton_notaria);
                        continue;
                    }
                    //return $notaria;
                    $ff=str_replace("/","-",trim($value->fecha_inscripcion));
                    $repertorio=intval(explode(",",trim($value->numero_repertorio))[0]);
                    $inscripcion=intval(explode(",",trim($value->numero_inscripcion))[0]);
                    $verificar=InscripcionModel::where(function($query)use($repertorio,$inscripcion,$ff,$libro){
                        $query->where("NroRepertorio",$repertorio)
                        ->where("NroInscripcion",$inscripcion)
                        ->where(DB::raw("to_char(\"FechaInscr\", 'YYYY')"),date("Y",strtotime($ff)))
                        ->where("IdLibro",$libro->OID);
                    })->first();  
                    //$this->showdata(array("repertorio"=>$repertorio,"incrip"=>$inscripcion,$verificar));                  
                    if(!empty($verificar))
                    {
                        $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$value->id_inscripcion. " YA EXISTE ESTA INSCRIPCIÓN","idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
                        $this->actualizarestadocab(0,$value->id,"NO PROCESADO","YA EXISTE ESTA INSCRIPCIÓN",-1);
                        $idmainexis=$verificar->OID;
                        $savebus=InscripcionDetalleModel::where("id_inscripcion",trim($value->id_inscripcion))->get();
                        foreach($savebus as $keybus => $valuebus) {
                            # code...
                            $save=InscripcionDetalleModel::find($valuebus->id);
                            $save->idcab=$idmainexis;
                            $save->save();
                            $detainter="";
                            unset($save);
                        }                   
                        continue;
                    }
                    //die();
                    $guardar= new InscripcionModel;
                    //$guardar->FechaInscr=date("Y-m-d",strtotime("2018-05-22"))." ".date("H:i:s"); //date("Y-m-d H:i:s");//date("Y-m-d",strtotime(str_replace("/","-",$value->fecha_inscripcion)));
                    //$guardar->FechaInscr=date("Y-m-d",strtotime("2018-05-22"))." ".date("H:i:s"); //date("Y-m-d H:i:s");//date("Y-m-d",strtotime(str_replace("/","-",$value->fecha_inscripcion)));                    
                    /*if(strlen($ff)<10)
                    {
                        $ff="";
                    }*/
                    $guardar->FechaInscr=date("Y-m-d",strtotime($ff));
                    $guardar->NrosAutogenerados=false;
                    $guardar->NroRepertorio=trim($repertorio);
                    $guardar->IdLibro=$libro->OID;
                    $guardar->NroInscripcion=trim($inscripcion);
                    $guardar->IdActo=$actosbus->Actos;
                    $guardar->IdCanton=$canton1->OID;
                    //$guardar->CodLibro=
                    //$guardar->NroTomo=                    
                    $guardar->FolioInicial=intval($value->folio_inicial);
                    $guardar->FolioFinal=intval($value->folio_final);
                    //$guardar->NroSecuencia=
                    $guardar->IdNotaria=$notaria->OID;
                    $guardar->CantonNotaria=$canton2->OID;
                    if(trim($value->fecha_otorgamiento)!="")
                        $guardar->FechaOtorga=date("Y-m-d",strtotime(str_replace("/","-",$value->fecha_otorgamiento)));
                    $guardar->AfiliadoCamara=false;
                    $guardar->MovtoOrdenJudc=false;
                    if(trim($value->fecha_resolucion)!="")
                        $guardar->FechaResolucion=date("Y-m-d",strtotime(str_replace("/","-",$value->fecha_resolucion)));                    
                    //$guardar->NroResolucion=
                    $guardar->FechaDocto=date("Y-m-d");
                    $guardar->FechaEscritura=date("Y-m-d");
                    $guardar->IdDivisa=1;
                    $guardar->Cuantia=floatval($value->valor_cuantia);
                    $guardar->Vigente=false;
                    $guardar->Activado=true;
                    $guardar->Printed=false;
                    $guardar->CreadoPor=$this->creadopor;
                    $guardar->Compania=1;
                    $guardar->Naturaleza_Acto="";                    
                    $guardar->save();
                    /**/
                    $idmain=$guardar->OID;                    
                    /*OBSERVACION*/
                    if(trim($value->observacion)!="")
                    {
                        $observa=new ObservacionesModel;
                        //insert into "Observaciones_Inscrp"("FechaIngreso","IdInscripcion","Observacion","Activa","ImprimirEn","IdUserCrea","OptimisticLockField","GCRecord")
                        $observa->FechaIngreso=date("Y-m-d H:i:s");//date("Y-m-d",strtotime("2018-05-22"))." ".date("H:i:s");//date("Y-m-d H:i:s");
                        $observa->IdInscripcion=$idmain;
                        $observa->Observacion=$value->observacion;
                        $observa->Activa=true;
                        $observa->IdUserCrea=$this->creadopor;
                        $observa->ImprimirEn=0;
                        $observa->OptimisticLockField=2;
                        $observa->save(); 
                    }
                    /*Procesa Intervinienetes*/
                    $detainter=". No existe Intervinientes para este registro";
                    $savebus=InscripcionDetalleModel::where("id_inscripcion",trim($value->id_inscripcion))->get();
                    $st_estado=0;
                    foreach($savebus as $keybus => $valuebus) {
                        # code...
                        $save=InscripcionDetalleModel::find($valuebus->id);
                        $save->idcab=$idmain;
                        $save->save();
                        $detainter="";
                        unset($save);
                        $st_estado=$idmain;
                    }                   
                    $procesados[]=array("id"=>$value->id,"estado"=>"PROCESADO","detalleproceso"=>$value->id_inscripcion. " PROCESADO EXITOSAMENTE OK".$detainter,"idokmain"=>$idmain,"fechaproceso"=>date("Y-m-d H:i:s"));
                    /*==========================================*/
                    #Actualizar Estado de Propceso
                    //actualizarestadocab($idproceso,$id,$estado="PROCESADO",$obs="Procesado exitosamente")
                    $this->actualizarestadocab($idmain,$value->id,"PROCESADO","Procesado exitosamente".$detainter,$st_estado);
                    $this->savehistorial($idmain,"Procesado exitosamente".$detainter);
                }
                //DB::rollback();
                DB::commit();        
            }catch(\Exception $e){
                // ROLLBACK
                DB::rollback();
                //$procesados[]=array("Error"=>$e->getMessage(). " ".$e->getLine(),"observa"=>$value->id_inscripcion,"id_main"=>$idmain);
                //$procesados[]=array("Error"=>$e->getMessage(). " ".$e->getLine(),"observa"=>$value->id_inscripcion,"id_main"=>$idmain);
                $procesados[]=array("id"=>$value->id,"estado"=>"NO PROCESADO","detalleproceso"=>$e->getMessage(). " ".$e->getLine(),"idokmain"=>0,"fechaproceso"=>date("Y-m-d H:i:s"));
            }
        return view('vistas.resulcab',["procesados"=>$procesados,"totalpendiente"=>$totalpendiente,"totalproce"=>$totalproce,"porcentaje"=>$porcentaje,"totalmain"=>$totalmain]);
        return $procesados;//$mensaje;//$procesados;
        //return $mensaje;
        //return $tabla->toarray();
    }
    public function indexdeta()
    {
        return view('vistas.sincronizacionhomedeta');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vistas.sincronizacionhome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
