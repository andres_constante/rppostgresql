<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;
use DB;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\RecaudacionModel;
use App\UsuariosModel;
use App\PerfilModel;
use App\MercadoModel;
use App\SeccionModel;
use App\ComercianteModel;

class ReporteController extends Controller
{
    //
    var $configuraciongeneral = array ("Reporte de Recaudación", "reportes", "index"); 
    var $objetos = '[ 
    			  {"Tipo":"select","Descripcion":"Mercado","Nombre":"idmercado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
                  {"Tipo":"datetextdisabled","Descripcion":"Fecha","Nombre":"fecha","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Sección","Nombre":"idseccion","Clase":"selective-normal","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Comerciante","Nombre":"idcomerciante","Clase":"selective-normal","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Valor","Nombre":"valor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Usuario","Nombre":"idusuario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
                  ]';   
    var $combos = '[ 
    			  {"Tipo":"select","Descripcion":"Tipo Reporte","Nombre":"filtro1","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },                   
                  {"Tipo":"select","Descripcion":"Opción","Nombre":"filtro2","Clase":"selective-normal","Valor":"Null","ValorAnterior" :"Null"}                  
                  ]';

     var $inputs='[{"Tipo":"datetext","Descripcion":"Fecha Inicio","Nombre":"fecha1","Clase":"Null","Valor":"Null"},
                  {"Tipo":"datetext","Descripcion":"Fecha Fin","Nombre":"fecha2","Clase":"Null","Valor":"Null"}]';

    var $escoja=array(null=>"Escoja opción...") ;
//https://jqueryvalidation.org/validate/    
    public function __construct() {
        $this->middleware('auth');
    } 
    public function obtenertipo(){
    	$id= Input::get("id");
    	if($id==0)
    	{
    		$tabla=ComercianteModel::where("estado","ACT")->lists("nombre","id")->all();
    	}elseif($id==1)
    	{
    		$tabla=MercadoModel::lists("descripcion","id")->all();    		        
    	}elseif($id==2){
    		$tabla=SeccionModel::lists("nombre","id")->all();
    	}elseif($id==3){
    		$tabla=UsuariosModel::lists("name","id")->all();
    	}
    	return $tabla;
    }
    public function index(){
    	$filtro1=1;
    	$filtro2=-1;
    	$filtro3=-1;
    	$filtro4=-1;
    	if(Input::has("filtro1"))
    	{
    		$filtro1=Input::get("filtro1");
    		$filtro2=Input::get("filtro2");
    		$filtro3=Input::get("filtro3");
    		$filtro4=Input::get("filtro4");
    	}

    	$inputs=json_decode($this->inputs);
    	$combos=json_decode($this->combos);
    	$combos[0]->Valor=$this->escoja + array("Comerciante","Mercado","Sección","Recaudador");
    	$combos[1]->Valor=array();
    	

    	$objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="mercado";
        $objetos[2]->Nombre="seccion";
        $objetos[3]->Nombre="nombre";
        $objetos[5]->Nombre="usuario";
        /*unset($objetos[5]);
        unset($objetos[6]);
        $objetos=array_values($objetos);
        */
        //show($objetos);
        if ($filtro1==1 && $filtro2==-1 && $filtro3==-1 && $filtro4==-1) {
        	# code...
        	 $tabla = RecaudacionModel::join("seccion as a","a.id","=","pagos.idseccion")
	        	->join("comerciante as b","b.id","=","pagos.idcomerciante")
	        	->join("mercado as c","c.id","=","pagos.idmercado")
	        	->join("users as d","d.id","=","idusuario")
	        	->select("pagos.*","a.nombre as seccion","b.nombre","c.descripcion as mercado","d.name as usuario")
	        	->where("pagos.estado","ACT");
        }else{
        	 $tabla = RecaudacionModel::join("seccion as a","a.id","=","pagos.idseccion")
	        	->join("comerciante as b","b.id","=","pagos.idcomerciante")
	        	->join("mercado as c","c.id","=","pagos.idmercado")
	        	->join("users as d","d.id","=","idusuario")
	        	->select("pagos.*","a.nombre as seccion","b.nombre","c.descripcion as mercado","d.name as usuario")
	        	->where("pagos.estado","ACT");
		        if($filtro1==0){
		        	$tabla->where("b.id","=",$filtro2)
		        	->whereBetween("pagos.fecha",[$filtro3,$filtro4]);       
		        }elseif($filtro1==1)
		        {
		        	$tabla->where("c.id","=",$filtro2)
		        	->whereBetween("pagos.fecha",[$filtro3,$filtro4]);   
		        }elseif($filtro1==2)
		        {
		        	$tabla->where("a.id","=",$filtro2)
		        	->whereBetween("pagos.fecha",[$filtro3,$filtro4]);   
		        }elseif($filtro1==3){
		        	$tabla->where("d.id","=",$filtro2)
		        	->whereBetween("pagos.fecha",[$filtro3,$filtro4]);   
		        }
		    }      

       
        $suma=$tabla->sum("pagos.valor");
        //show($suma);
		$tabla=$tabla->get();
        return view('reportes.index',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "sumatotal"=>$suma, 
                "combos"=>$combos,
                "inputs"=>$inputs,
                ]);
    }
}
