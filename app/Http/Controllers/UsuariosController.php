<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\TipoUsuarioModel;
use App\UsuariosModel;
use App\PerfilModel;
use DB;
class UsuariosController extends Controller
{
    var $configuraciongeneral = array ("Usuarios del Sistema", "usuarios", "index");    
    var $objetos = '[ {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
                    {"Tipo":"text","Descripcion":"Nombres","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Teléfono","Nombre":"telefono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Email","Nombre":"email","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"datetext","Descripcion":"Fecha de Nacimiento","Nombre":"fecha_nacimiento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"password","Descripcion":"Contraseña","Nombre":"password","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"password","Descripcion":"Confirmar Contraseña","Nombre":"password_confirmation","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Tipo de usuario","Nombre":"id_tipo_usuario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Perfil","Nombre":"id_perfil","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
                  ]';
    var $escoja=array(null=>"Escoja opción...") ;
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "cedula"=>"cedula: {
                            required: true
                        }",
            "name"=>"name: {
                            required: true
                        }",
            "telefono"=>"telefono: {
                            required: true
                        }",
            "email"=>"email: {
                            required: true
                        }",
            "fecha_nacimiento"=>"fecha_nacimiento: {
                            required: true
                        }",
            "password"=>"password: {
                            required: true
                        }",
            "password_confirmation"=>"password_confirmation: {
                            required: true
                        }",
            "id_tipo_usuario"=>"id_tipo_usuario: {
                            required: true
                        }"

        );
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objetos=json_decode($this->objetos);
        $objetos[7]->Nombre="tipo_usuario";
        $objetos[8]->Nombre="perfil";
        unset($objetos[5]);
        unset($objetos[6]);
        $objetos=array_values($objetos);
        //show($objetos);
        $tabla = UsuariosModel::join("ad_tipousuario as c","users.id_tipo_usuario","=","c.id")
                    ->select("users.*","c.tipo_usuario",
                            DB::raw("(select perfil from ad_perfil where id=users.id_perfil) as perfil"))
                    ->where("users.estado","ACT")
                    ->get();
             return view('vistas.index',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si"
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        $tipous=TipoUsuarioModel::lists("tipo_usuario","id")->all();
        $perfil=PerfilModel::lists("perfil","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[7]->Valor=$this->escoja + $tipous;
        $objetos[8]->Valor=$this->escoja + $perfil;
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new UsuariosModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= UsuariosModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, UsuariosModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                unset($input["password_confirmation"]);
                //show($input);
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        if($key=="password")
                        {
                            $guardar->$key = bcrypt($value);
                        }else{
                            $guardar->$key = $value;
                        }
                    }                        
                 }

                 $guardar->save();
                 /*Permisos por Perfil*/
                        $perfil= Input::get('id_perfil');
                        if($perfil)
                        {
                            $usuario=$guardar->id;
                            $permiper= \App\PerfilPermisosModel::where("idperfil",$perfil)->get();
                            AsignarPermisosModel::where('idusuario', '=', $usuario)->delete();
                            foreach ($permiper as $key => $value) {
                                $permisos= new \App\AsignarPermisosModel();
                                $permisos->idusuario  = $usuario;
                                $permisos->idmenu     = $value->idmenu;
                                $permisos->save();
                            }
                        }
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->cedula));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos=json_decode($this->objetos);
        $objetos[7]->Nombre="tipo_usuario";
        $objetos[8]->Nombre="perfil";
        unset($objetos[5]);
        unset($objetos[6]);
        $objetos=array_values($objetos);
        //show($objetos);
        $tabla = UsuariosModel::join("ad_tipousuario as c","users.id_tipo_usuario","=","c.id")
                    ->select("users.*","c.tipo_usuario",
                            DB::raw("(select perfil from ad_perfil where id=users.id_perfil) as perfil"))
                    ->where("users.id",$id)
                    ->first();
        return view('vistas.show',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tipous=TipoUsuarioModel::lists("tipo_usuario","id")->all();
        $perfil=PerfilModel::lists("perfil","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[7]->Valor=$this->escoja + $tipous;
        $objetos[8]->Valor=$this->escoja + $perfil;
        $tabla=UsuariosModel::find($id);
        return view('vistas.create',[
                "tabla"=>$tabla,
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=UsuariosModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
