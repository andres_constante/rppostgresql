<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\PerfilModel;

class PerfilController extends Controller
{
    var $configuraciongeneral = array ("Perfiles de usuario", "perfil", "index");
    var $objetos = '[ {"Tipo":"text","Descripcion":"Perfil","Nombre":"perfil","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "perfil"=>"perfil: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla=PerfilModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
        return view('vistas.index',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si"
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new PerfilModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= PerfilModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, PerfilModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }

                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->perfil));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = PerfilModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = PerfilModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=PerfilModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
