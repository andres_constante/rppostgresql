<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//
use App\Http\Controllers\Controller;
use App\SeccionModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

class SeccionController extends Controller
{
    //
	var $configuraciongeneral = array ("Sección", "seccion", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Sección","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"text","Descripcion":"Valor Tarifa","Nombre":"valor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                  ]'; 
    var $validarjs =array(
            "nombre"=>"nombre: {
                            required: true
                        }",
             "valor"=>"valor: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla=SeccionModel::orderby("id","desc")->get();//->paginate(500);
        return view('vistas.index',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new SeccionModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro de Sección";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= SeccionModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición de Sección";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, SeccionModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }

                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$guardar->id. "-".Input::get($guardar->nombre));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = SeccionModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = SeccionModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs,
                "tabla"=>$tabla
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        /*$tabla = ComercianteModel::find($id);
        $tabla->estado="INA";
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
        */
    }
}
