<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\UsuariosModel;
class CambiaDatosController extends Controller
{
        var $configuraciongeneral = array ("Datos del Usuario", "cambiardatos", "editar");
    var $objetos = '[ {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
                    {"Tipo":"text","Descripcion":"Nombres","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Teléfono","Nombre":"telefono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Email","Nombre":"email","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"datetext","Descripcion":"Fecha de Nacimiento","Nombre":"fecha_nacimiento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"password","Descripcion":"Contraseña","Nombre":"password","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"password","Descripcion":"Confirmar Contraseña","Nombre":"password_confirmation","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            /*"campo"=>"campo: {
                            required: true
                        }",
            "valor"=>"valor: {
                            required: true
                        }"*/
        );
    public function __construct() {
        $this->middleware('auth');
    } 
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //show(json_decode($this->objetos));
        $tabla=UsuariosModel::find($id);
        //show($tabla);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new UsuariosModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= UsuariosModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, UsuariosModel::rulescon($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }
                 $guardar->nuevo=0;
                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->cedula));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
