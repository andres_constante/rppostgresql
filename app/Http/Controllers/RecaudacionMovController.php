<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

use App\RecaudacionModel;
use App\UsuariosModel;
use App\PerfilModel;
use App\MercadoModel;
use App\SeccionModel;
use App\ComercianteModel;
use Auth;

class RecaudacionMovController extends Controller
{
    //
    var $configuraciongeneral = array ("Recaudación Mercados", "recaudacionmov", "index");    
    var $objetos = '[ 
    			  {"Tipo":"select","Descripcion":"Mercado","Nombre":"idmercado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
                  {"Tipo":"textdisabled","Descripcion":"Fecha","Nombre":"fecha","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Sección","Nombre":"idseccion","Clase":"selective-normal","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Comerciante","Nombre":"idcomerciante","Clase":"selective-normal","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Valor","Nombre":"valor","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Usuario","Nombre":"idusuario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
                  ]';
    var $escoja=array(null=>"Escoja opción...") ;
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "idmercado"=>"idmercado: {
                            required: true
                        }",
            "fecha"=>"fecha: {
                            required: true
                        }",
            "idseccion"=>"idseccion: {
                            required: true
                        }",
            "idcomerciante"=>"idcomerciante: {
                            required: true
                        }",
            "valor"=>"valor: {
                            required: true
                        }"

        );
    public function __construct() {
        $this->middleware('auth');
    } 
    function deudacontribuyente(){
    	$id=Input::get("id");
    	$tabla=ComercianteModel::find($id);
    	return floatval($tabla->valor_adeudado);
    }
    function valortasa(){
    	$id=Input::get("id");
    	$tabla=SeccionModel::find($id);
    	return floatval($tabla->valor);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Redirect::to($this->configuraciongeneral[1]."/create");
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="mercado";
        $objetos[2]->Nombre="seccion";
        $objetos[3]->Nombre="nombre";
        $objetos[5]->Nombre="usuario";
        /*unset($objetos[5]);
        unset($objetos[6]);
        $objetos=array_values($objetos);
        */
        //show($objetos);
        $tabla = RecaudacionModel::join("seccion as a","a.id","=","pagos.idseccion")
        	->join("comerciante as b","b.id","=","pagos.idcomerciante")
        	->join("mercado as c","c.id","=","pagos.idmercado")
        	->join("users as d","d.id","=","idusuario")
        	->select("pagos.*","a.nombre as seccion","b.nombre","c.descripcion as mercado","d.name as usuario")
        	->where("pagos.estado","ACT");
        $suma=$tabla->sum("pagos.valor");
        //show($suma);
		$tabla=$tabla->get();
        return view('vistas.index',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "sumatotal"=>$suma,
                "delete"=>"si"
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        $tb1=MercadoModel::lists("descripcion","id")->all();
        $tb2=SeccionModel::lists("nombre","id")->all();
        $tb3=ComercianteModel::where("estado","ACT")->lists("nombre","id")->all();
        $objetos=json_decode($this->objetos);
        unset($objetos[5]);
        $objetos[0]->Valor=$this->escoja + $tb1;
        $objetos[1]->Valor=fechas(1);
        $objetos[2]->Valor=$this->escoja + $tb2;
        $objetos[3]->Valor=$this->escoja + $tb3;
        //show($objetos);
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         
           $input=Input::all();
           //show($input);
            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new RecaudacionModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Pago";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= RecaudacionModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Pago";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, RecaudacionModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {                
                //show($input);
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                            $guardar->$key = $value;
                    }                        
                 }
                 $guardar->idusuario=Auth::user()->id;                 
                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->valor));   
                 /*Actualizar deuda*/
                 $tabla=ComercianteModel::find($guardar->idcomerciante);
    			$valor=floatval($tabla->valor_adeudado);
    			if($valor!=0)
    			{
    				$valor= $valor - floatval($guardar->valor);	
    				if($valor<0)
    					$valor=0;
    				$tabla->valor_adeudado=$valor;
    				$tabla->save();
    			}
    			
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]."/create");
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="mercado";
        $objetos[2]->Nombre="seccion";
        $objetos[3]->Nombre="nombre";
        $objetos[5]->Nombre="usuario";
        /*unset($objetos[5]);
        unset($objetos[6]);
        $objetos=array_values($objetos);
        */
        //show($objetos);
        $tabla = RecaudacionModel::join("seccion as a","a.id","=","pagos.idseccion")
        	->join("comerciante as b","b.id","=","pagos.idcomerciante")
        	->join("mercado as c","c.id","=","pagos.idmercado")
        	->join("users as d","d.id","=","idusuario")
        	->select("pagos.*","a.nombre as seccion","b.nombre","c.descripcion as mercado","d.name as usuario")
            ->where("pagos.id",$id)
			->first();
        
        return view('vistas.show',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tb1=MercadoModel::lists("descripcion","id")->all();
        $tb2=SeccionModel::lists("nombre","id")->all();
        $tb3=ComercianteModel::lists("nombre","id")->all();
        $objetos=json_decode($this->objetos);
        unset($objetos[5]);
        $objetos[0]->Valor=$this->escoja + $tb1;
        $objetos[1]->Valor=fechas(1);
        $objetos[2]->Valor=$this->escoja + $tb2;
        $objetos[3]->Valor=$this->escoja + $tb3;
        $tabla=RecaudacionModel
        ::find($id);
        return view('vistas.create',[
                "tabla"=>$tabla,
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=RecaudacionModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
		Session::flash('message', 'Registro dado de Baja!');
		Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->valor));   
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
