<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;
use App\ComercianteModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

class ComercianteController extends Controller
{
    var $configuraciongeneral = array ("Comerciante", "comerciante", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Nombre","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }, 
        {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"text","Descripcion":"Teléfono","Nombre":"telefono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" },
        {"Tipo":"text","Descripcion":"Valor Adeudado","Nombre":"valor_adeudado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                  ]'; 
    var $validarjs =array(
            "nombre"=>"nombre: {
                            required: true
                        }",
            "cedula"=>"cedula: {
                            required: true
                        }",
            "telefono"=>"telefono: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla=ComercianteModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
        return view('vistas.index',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si"
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         
           $input=Input::all();
           //show($input);
            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new ComercianteModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro de Comerciante";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= ComercianteModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición de Comerciante";
            }

            //$input=Input::all();
            $arrapas=array();
            //show($id);
            $validator = Validator::make($input, ComercianteModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }

                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$guardar->id. "-".Input::get($guardar->cedula));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = ComercianteModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = ComercianteModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs,
                "tabla"=>$tabla
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla = ComercianteModel::find($id);
        $tabla->estado="INA";
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
