<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
 use App\PruebaModel;

 use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use App\InscripcionCabeceraSynModel;
use GuzzleHttp\Client as GuzzleHttpClient;
class PruebaController extends Controller
{
    //  
  
     var $configuraciongeneral = array ("Prueba", "pruebaregistro", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Nombre","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }, 
        {"Tipo":"text","Descripcion":"Edad","Nombre":"edad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                  ]'; 
    var $validarjs =array(
            "nombre"=>"nombre: {
                            required: true
                        }",
            "edad"=>"edad: {
                            required: true
                        }"
        );
    /*public function __construct() {
        $this->middleware('auth');
    } */
    public function pruebaconexion(){
      $tabla=InscripcionCabeceraSynModel::find(52071);
      return $tabla;
    }
    public function pruebaguzzle()
    {
      /*$pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML('<h1>Test</h1>');
      return $pdf->stream();
*/
      $client = new GuzzleHttpClient();
      $urlru="http://webserver.manta.gob.ec:9083/api/Deudas/Obtener_deuda_general/1309366514";
      $urlru="http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/1312389370/1015";
      //$urlru="http://webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/1312389370001/1021";
      try {
        $respuesta = $client->request('GET',$urlru);
        $deudasruc= json_decode($respuesta->getBody()->getContents(),true);
      } catch (\Exception $e) {
        show(array("error",$e->getMessage()));
        //return Redirect::to("$urlretorno")->withInput()->withErrors(["message"=>$e->getMessage()]);
      }
      show($deudasruc);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla=PruebaModel::orderby("id","desc")->get();//->paginate(500);
        return view('vistas.index',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si"
                ]);
    }
     public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         
           $input=Input::all();
           //show($input);
            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new PruebaModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro de Comerciante";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= PruebaModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición de Comerciante";
            }

            //$input=Input::all();
            $arrapas=array();
            //show($id);
            $validator = Validator::make($input, PruebaModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }

                 $guardar->save();
                 //Auditoria($msgauditoria." - ID: ".$guardar->id. "-".Input::get($guardar->cedula));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         return $this->guardar(0);
    }
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = PruebaModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs,
                "tabla"=>$tabla
                ]);
    }
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }
     public function show($id)
    {
        //
        $tabla = PruebaModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }
     public function destroy($id)
    {
        //
        $tabla = PruebaModel::find($id);
        $tabla->delete();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
