<?php

class SubirDocuController extends BaseController {
	public function pruebasubir()
	{            
                    ini_set('memory_limit','500M');
                    set_time_limit (0);
                    DB::disableQueryLog();
                    $min=DB::connection('migracion')
                            ->table("facturasjson")
                            ->min("id");
                    $max=DB::connection('migracion')
                            ->table("facturasjson")
                            ->max("id");
                    //die("$total $min $max");
                    $contents="";
                    for($i=$min;$i<=$max;$i++)
                    {
                        $datos=DB::connection('migracion')
                            ->table("facturasjson")
                            ->where("id",$i)
                            ->first();
                        if($datos)
                            $contents.=$datos->detalle;
                        //$contents= substr(trim($contents), 0, -1);
                        //break;
                    }
                    echo "<pre>";print_r($contents);
                    die();            
	}
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $tabla= array(
                "Facturas"=>"Facturas",
                "Retencion"=>"Comprobante de Retención",
                "Remision"=>"Guias de Remisión",
                "Credito"=>"Notas de Crédito",
                "Debito"=>"Notas de Débito",
                "Clientes"=>"Clientes",
                "Corrige"=>"Corrige Contraseña"
                );
            return View::make('documentos.index')->with("documentos",$tabla);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('subirdocus.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
            header('Content-Type: text/html; charset=utf-8'); 
            echo HTML::script('js/jquery-2.1.1.js');
            echo "<div id='resul'></div>";
            $input=Input::all();
            $rules = array(
                'documento'=>'required'
            );
            $validator = Validator::make($input, $rules);
		// process the login
		if ($validator->fails()) {
			return Redirect::to('cargardocumentos')
				->withErrors($validator)
				->withInput();//Esta parte es importante para q no borre los campos al validadr
                        //->withInput(Input::except('password'));
		} 
                $documento=Input::get('documento');
                $fechaini= MisFunciones::fechas(100);
                $total=0; 
                $mensaje="";
                $borrar=intval(Input::get('chkborrar'));
                //die($borrar);
                /**/

                try
                {
                    ini_set('memory_limit','500M');
                    set_time_limit (0);
                    switch ($documento)
                    {
                        case "Facturas": $filename = 'archivos/facturas.txt';
                            break;
                        case "Retencion": $filename = 'archivos/retenciones.txt';
                            break;
                        case "Remision": $filename = 'archivos/guiasremision.txt';
                            break;
                        case "Credito": $filename = 'archivos/notas_credito.txt';
                            break;
                        case "Debito": $filename = 'archivos/notas_debito.txt';
                            break;                        
                        default:
                            $filename="";
                    }
                    if ($filename=="" || !file_exists($filename))
                    {
                        $fechafin= MisFunciones::fechas(100);
                    //Session::flash('message', 'Propietario Creado!');
                    $mensaje.="No existe archivo para procesar... '$filename'";
                    return Redirect::to('cargardocumentos')
				->withErrors(["$mensaje"])
				->withInput();
                    }
                    $lineas = file($filename);
                    $contents="";
                    foreach ($lineas as $num_linea => $linea) 
                    {
                    $contents=$linea;
                    $coma= substr(trim($contents), -1);
                    if($coma==",")
                        $contents= substr(trim($contents), 0, -1);
                    //echo "<pre> $contents";
                    
                    //die();
                    $contents = utf8_encode($contents);
                    $json_data=json_decode("[".$contents."]");
                    //echo "<pre>";print_r($json_data);
                    //die();                    
                    if(!isset($json_data)){
                        $fechafin= MisFunciones::fechas(100);
                        $tiempo=strtotime($fechafin)-strtotime($fechaini);                    
                        $mensaje="El archivo json no tiene el formato correcto. Linea: $num_linea. Total Pocesados: $total. ".
                        "Inicio: $fechaini Fin: $fechafin. ".
                            "Tiempo Transcurrido: $tiempo secs.";
                        echo "$mensaje<pre>";print_r($contents);
                        die();
                        return Redirect::to('cargardocumentos')
				->withErrors(["$mensaje"])
				->withInput();//Esta parte es importante para q no borre los campos al validadr
                        //->withInput(Input::except('password'));
                    }
                    //echo "<pre>";
                    //var_dump($json_data);
                    //die();
                    if($borrar==1 && $i==$min){
//                        DB::table('facturas')->truncate();
                    }
                    foreach($json_data as $line =>$valor) {
                        $ok="";
                        if($documento=='Facturas')
                        {
                        //var_dump($valor);  
                        $guardar= FacturasModel::where("id_factura",$valor->id_factura)->first();
                        //var_dump($guardar);
                        //die();
                         //if($guardar->isEmpty())
                         if(!$guardar)
                         {
                             $xml=str_replace('&quot;', '"', $valor->XmlComprobanteFacturaAutorizado);
                             $xml=str_replace('&lt;', '<', $xml);
                             $xml=str_replace('&gt;', '>', $xml);
                             //$xml = utf8_decode($xml);                             
                            $guardar= new FacturasModel;
                            $guardar->id_factura= $valor->id_factura;
                            $guardar->id_comprobante= $valor->id_comprobante;
                            $guardar->NumeroAutorizacion= $valor->NumeroAutorizacion;
                            $guardar->ClaveContingencia= $valor->ClaveContingencia;
                            $guardar->FechaAutorizacion= $valor->FechaAutorizacion;
                            $guardar->FechaEmision= $valor->FechaEmision;
                            $guardar->ClienteId= $valor->ClienteId;
                            $guardar->ClaveAcceso= $valor->ClaveAcceso;
                            $guardar->FecIng= $valor->FecIng;
                            $guardar->XmlComprobanteFacturaAutorizado= $xml;
                            $guardar->bandera= $valor->bandera;
                            $guardar->email= $valor->email;
                            $guardar->secuencial= $valor->secuencial;
                            $guardar->save();
                            $total++;
                            $ok=" -> OK";
                         }
                         }//IF Factura
                         elseif ($documento=='Remision') 
                         {
                            $guardar= GuiasModel::where("id_guia_remision",$valor->id_guia_remision)->first();
                            if(!$guardar)
                            {
                             $xml=str_replace('&quot;', '"', $valor->XmlComprobanteFacturaAutorizado);
                             $xml=str_replace('&lt;', '<', $xml);
                             $xml=str_replace('&gt;', '>', $xml);
                             //$xml = utf8_decode($xml);
                            $guardar= new GuiasModel;
                            $guardar->id_guia_remision= $valor->id_guia_remision;
                            $guardar->id_comprobante= $valor->id_comprobante;
                            $guardar->NumeroAutorizacion= $valor->NumeroAutorizacion;
                            $guardar->ClaveContingencia= $valor->ClaveContingencia;
                            $guardar->FechaAutorizacion= $valor->FechaAutorizacion;
                            $guardar->FechaEmision= $valor->FechaEmision;
                            $guardar->ClienteId= $valor->ClienteId;
                            $guardar->ClaveAcceso= $valor->ClaveAcceso;
                            $guardar->FecIng= $valor->FecIng;
                            $guardar->XmlComprobanteFacturaAutorizado= $xml;
                            $guardar->bandera= $valor->bandera;
                            $guardar->secuencial= $valor->secuencial;
                            $guardar->save();
                            $total++;
                            $ok=" -> OK";                              
                            }
                         }//Fin Guias Remision
                         elseif ($documento=='Retencion') 
                        {
                        $guardar= RetencionesModel::where("id_retencion",$valor->id_retencion)->first();
                         if(!$guardar)
                         {
                             $xml=str_replace('&quot;', '"', $valor->XmlComprobanteFacturaAutorizado);
                             $xml=str_replace('&lt;', '<', $xml);
                             $xml=str_replace('&gt;', '>', $xml);
                             //$xml = utf8_decode($xml);
                            $guardar= new RetencionesModel;
                            $guardar->id_retencion= $valor->id_retencion;
                            $guardar->id_comprobante= $valor->id_comprobante;
                            $guardar->NumeroAutorizacion= $valor->NumeroAutorizacion;
                            $guardar->ClaveContingencia= $valor->ClaveContingencia;
                            $guardar->FechaAutorizacion= $valor->FechaAutorizacion;
                            $guardar->FechaEmision= $valor->FechaEmision;
                            $guardar->ClienteId= $valor->ClienteId;
                            $guardar->ClaveAcceso= $valor->ClaveAcceso;
                            $guardar->FecIng= $valor->FecIng;
                            $guardar->XmlComprobanteFacturaAutorizado= $xml;
                            $guardar->bandera= $valor->bandera;
                            $guardar->secuencial= $valor->secuencial;
                            $guardar->save();
                            $total++;
                            $ok=" -> OK";
                         }                                 
                         }//FIn Retencion
                         elseif ($documento=='Credito') 
                         {
                        $guardar= NotasCreModel::where("id_nota_credito",$valor->id_nota_credito)->first();
                         if(!$guardar)
                         {
                             $xml=str_replace('&quot;', '"', $valor->XmlComprobanteFacturaAutorizado);
                             $xml=str_replace('&lt;', '<', $xml);
                             $xml=str_replace('&gt;', '>', $xml);
                             //$xml = utf8_decode($xml);
                            $guardar= new NotasCreModel;
                            $guardar->id_nota_credito= $valor->id_nota_credito;
                            $guardar->id_comprobante= $valor->id_comprobante;
                            $guardar->NumeroAutorizacion= $valor->NumeroAutorizacion;
                            $guardar->ClaveContingencia= $valor->ClaveContingencia;
                            $guardar->FechaAutorizacion= $valor->FechaAutorizacion;
                            $guardar->FechaEmision= $valor->FechaEmision;
                            $guardar->ClienteId= $valor->ClienteId;
                            $guardar->ClaveAcceso= $valor->ClaveAcceso;
                            $guardar->FecIng= $valor->FecIng;
                            $guardar->XmlComprobanteFacturaAutorizado= $xml;
                            $guardar->bandera= $valor->bandera;
                            $guardar->secuencial= $valor->secuencial;
                            $guardar->save();
                            $total++;
                            $ok=" -> OK";
                         }                                    
                         }//Notas de Credito
                        elseif ($documento=='Debito') 
                         {
                        $guardar= NotasCreModel::where("id_nota_debito",$valor->id_nota_debito)->first();
                         if(!$guardar)
                         {
                             $xml=str_replace('&quot;', '"', $valor->XmlComprobanteFacturaAutorizado);
                             $xml=str_replace('&lt;', '<', $xml);
                             $xml=str_replace('&gt;', '>', $xml);
                             //$xml = utf8_decode($xml);
                            $guardar= new NotasCreModel;
                            $guardar->id_nota_credito= $valor->id_nota_debito;
                            $guardar->id_comprobante= $valor->id_comprobante;
                            $guardar->NumeroAutorizacion= $valor->NumeroAutorizacion;
                            $guardar->ClaveContingencia= $valor->ClaveContingencia;
                            $guardar->FechaAutorizacion= $valor->FechaAutorizacion;
                            $guardar->FechaEmision= $valor->FechaEmision;
                            $guardar->ClienteId= $valor->ClienteId;
                            $guardar->ClaveAcceso= $valor->ClaveAcceso;
                            $guardar->FecIng= $valor->FecIng;
                            $guardar->XmlComprobanteFacturaAutorizado= $xml;
                            $guardar->bandera= $valor->bandera;
                            $guardar->secuencial= $valor->secuencial;
                            $guardar->save();
                            $total++;
                            $ok=" -> OK";
                         }                                    
                         }//Notas de Debito                         
                         $guardar=null;
                         //break;
                    }
                    $json_data=null;                    
                    $guardar=null;
                    $xml=null;
                    $contents=null;
                    echo "<script>";
                    //echo "Procesando '$filename' línea #: ".$num_linea."$ok<br>";
                    echo "$('#resul').html(\"<h2>Procesando '$filename' línea #: ".$num_linea."$ok </h2>\");";
                    echo "</script>";
                    echo "Procesando '$filename' línea #: ".$num_linea."$ok<br>";
                    }//Fin For recorre id tabla
                }
                catch (Illuminate\Filesystem\FileNotFoundException $exception)
                {
                    $mensaje="No existe el archivo";
                    $total=0;
                }
                if($total==0)
                {
                    $fechafin= MisFunciones::fechas(100);
                    //Session::flash('message', 'Propietario Creado!');
                    $mensaje.="No existe el archivo '$filename' para procesar o ya fue procesado...";                                    
                    return Redirect::to('cargardocumentos')
				->withErrors(["$mensaje"])
				->withInput();
                }else{
                    $fechafin= MisFunciones::fechas(100);
                    $tiempo=strtotime($fechafin)-strtotime($fechaini);
                    //Session::flash('message', 'Propietario Creado!');
                    $mensaje="<strong>Total Pocesados:</strong> $total<br>".
                        "<strong>Inicio:</strong> $fechaini <br><strong>Fin:</strong> $fechafin ".
                            "<br><strong>Tiempo Transcurrido:</strong> $tiempo secs.";                
                    //Session::flash('message', "Archivo '$filename' procesado.<br>".$mensaje );
                    Session::flash('message', "Listo.<br>".$mensaje );
                    return Redirect::to('cargardocumentos');                    
                }

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('subirdocus.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('subirdocus.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
