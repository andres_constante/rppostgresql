<?php
//use Request;
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>@yield('titulo')</title>
   
    {!! HTML::style('css/bootstrap.min.css') !!}       
    {!! HTML::style('font-awesome/css/font-awesome.css') !!}
 
   <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.responsive.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}    

    {{ HTML::style('https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css') }}
    {{ HTML::style('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}
       
<!-- Mensaje -->

{{ HTML::style('css/plugins/toastr/toastr.min.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}

    @yield('estilos')
<!-- Mainly scripts -->
{{ HTML::script('js/jquery-2.1.1.js') }} 
{{ HTML::script('js/bootstrap.min.js') }} 
{{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }} 
{{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }} 


<!-- Custom and plugin javascript -->
{{ HTML::script('js/inspinia.js') }} 
{{ HTML::script('js/plugins/pace/pace.min.js') }} 

 <!-- Data Tables -->
<!--
 {HTML::script('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js')}}
 {HTML::script('https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js')}}
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 {HTML::script('//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js')}}
 { HTML::script('js/plugins/dataTables/jquery.dataTables.js') }}     
    { HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }} 
    { HTML::script("js/plugins/dataTables/dataTables.responsive.js") }} 
    { HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }} 

-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>



    



 <!-- Mensaje -->
 {{ HTML::script('js/plugins/toastr/toastr.min.js') }}     
 
      <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}  
    <!-- Graficos estadissticos -->
{{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}
{{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }}
{{ HTML::script('js/plugins/morris/morris.js') }}
<!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}
    
    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
<!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}    
    
    {{ HTML::script('colorbox/jquery.colorbox.js') }} 
    <!-- Jquery Validate -->   
    {{ HTML::script('js/jquery.validate.min.js') }}   

    <!-- DatePicker -->    
    {{ HTML::style('bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ HTML::script('bootstrap-datepicker/bootstrap-datepicker.js') }}   
    {{ HTML::script('bootstrap-datepicker/locales/bootstrap-datepicker.es.js') }}   
    <!-- Selective -->
    {!! HTML::style('selective/selectize.bootstrap3.css'); !!}
    {!! Html::script('selective/selectize.js') !!}
    <!-- Tree -->
    {!! HTML::style('css/plugins/jsTree/style.css'); !!}
    {!! Html::script('js/plugins/jsTree/jstree.min.js') !!}


@yield('scripts')
 <script>
 function cargaatender()
 {
	 
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificatodos") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txttodos").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txttodos").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txttodos").html("<div class='alert alert-danger'>No existe URL</div>");
		    }	
			}	  
    }); 	 
 }
 function NotificacionTotal()
 {
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificacuenta") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txtcontar").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txtcontar").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txtcontar").html("<div class='alert alert-danger'>No existe URL</div>");
		    }	
			}	  
    }); 
 	 //setTimeout(NotificacionTotal, 5000);
 }
 //setTimeout(NotificacionTotal, 1000);


 </script>
</head>

<body>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    
                </li>
                <li {{ (Request::is('/') ? 'class=active' : '') }}>
                    <a href="{{ URL::to('/') }}"><i class="fa fa-home"></i> <span class="nav-label">Inicio</span></a>
                </li>  
                <!--li { (Request::is('sincronizacionhome') ? 'class=active' : '') }}>
                        <a href="{ URL::to('sincronizacionhome') }}"><i class="fa fa-flask"></i> <span class="nav-label">Sincronización</span></a>
                 </li-->
                <li {{ (Request::is('sincronizacionhome*') ? 'class=active' : '') }}>
                    <a href="#"><i class="fa fa-flask"></i> <span class="nav-label">Sincronización</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li {{ (Request::is('sincronizacionhome') ? 'class=active' : '') }}><a href="{{ URL::to('sincronizacionhome') }}">Inscripciones</a></li>
                        <li {{ (Request::is('sincronizacionhomedeta') ? 'class=active' : '') }}><a href="{{ URL::to('sincronizacionhomedeta') }}">Intervinientes</a></li>                        
                    </ul>
                </li>
                <li {{ (Request::is('listar*') ? 'class=active' : '') }}>
                    <a href="#"><i class="fa fa-building"></i> <span class="nav-label">Datos</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li {{ (Request::is('listarcabeceras') ? 'class=active' : '') }}><a href="{{ URL::to('listarcabeceras') }}">Inscripciones</a></li>
                        <li {{ (Request::is('listardetalles') ? 'class=active' : '') }}><a href="{{ URL::to('listardetalles') }}">Intervinientes</a></li>                        
                    </ul>
                </li>
                <li {{ (Request::is('estadistica') ? 'class=active' : '') }}>
                    <a href="{{ URL::to('estadistica') }}"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Estadística</span></a>
                </li> 
                <li {{ (Request::is('reporteingresos') ? 'class=active' : '') }}>
                    <a href="{{ URL::to('reporteingresos') }}"><i class="fa fa-bars"></i> <span class="nav-label">Inscripciones Procesadas</span></a>
                </li>  
                
            </ul>

        </div>
    </nav>
<div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <!--form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form-->
                </div>
                <ul class="nav navbar-top-links navbar-right">
					<!-- Alerta Campana -->					
					@if (Session::has('tipousuario') && Session::get('tipousuario')=="ADMINISTRADOR")
					<li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="javascript::" onclick="cargaatender()">
                        <i class="fa fa-bell"></i>  <span class="label label-primary" id="txtcontar">0</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" id="txttodos">
					
                    </ul>
                </li>
				@endif				
                    <li>
                        
                        <a href="#">
                            <i class="fa fa-user"></i> Administrador
                        </a>
                        
                    </li>                     
                    
                </ul>
            </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">                
                <div class="col-lg-12" id="divimprimir">
                        @yield('contenido')
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                <!--10GB of <strong>250GB</strong> Free. -->
                Administrador
            </div>
            <div>               
                {{ trans('html.main.copyright') }}
            </div>
        </div>

    </div>
</div>

</body>

</html>
