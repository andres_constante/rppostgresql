<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('titulo')</title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('font-awesome/css/font-awesome.css') }}
    
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}

<!-- Mainly scripts -->
{{ HTML::script('/js/jquery-2.1.1.js') }} 
{{ HTML::script('/js/bootstrap.min.js') }}     
</head>

<body class="gray-bglogin">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

               <!-- logomanta.jpg -->
                <h1 class="logo-name"> 
                    {{ HTML::image('img/logomanta.png','LOGO',array('style'=>'width: 180px; margin-bottom: 30px;')) }} 
                    </h1>


            </div>
            <h3 class="titulo">{{ trans('html.main.sistema') }}</h3>
          
            <p class="tab">@yield('cabecera')</p>
                           @yield('errores')
            <!--form class="m-t" role="form" action="index.html"-->
                @yield('formulario')
            <p class="m-tLogin"> <small>{{ trans('html.main.copyright') }}</small> </p>
        </div>
    </div>


</body>

</html>
