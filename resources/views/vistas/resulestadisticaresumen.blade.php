@extends ('layout')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')

@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF">Estadística de Migraciones - Resumen</h1>
<div class="ibox float-e-margins">
	<div class="ibox-title"> 
    	@if (Session::has('message'))
    		<script>    
			$(document).ready(function() {
    			//toastr.succes("{{ Session::get('message') }}");
			    toastr["success"]("{{ Session::get('message') }}");
			    //$.notify("{{ Session::get('message') }}","success");
			});
			</script>    
     		<div class="alert alert-info">{{ Session::get('message') }}</div>
    	@endif                          
			<!--div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
            </div-->
            {{ Form::open(array('url' => 'estadisticaresumen','role'=>'form' ,'class'=>'form-horizontal')) }}
<div class="panel-group" id="accordion" style="width: 400px;">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" >
                    <i class="fa fa-search fa-fw"></i>Filtro de Fechas</a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse {{ $colapse }}">
        <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>Desde:</th><td>{{ Form::input('date', 'desde', $fecha1, ['class' => 'form-control', 'placeholder' => 'Date']) }}</td>
                    </tr>
                    <tr>
                    <th>Hasta:</th><td>{{ Form::input('date', 'hasta', $fecha2, ['class' => 'form-control', 'placeholder' => 'Date']) }}</td>
                        <td rowspan="2"> {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}</td>
                    </tr>                                        
                </table>
        </div>
        </div>
    </div>                            
</div>

{{ Form::close() }} 	
	</div>
	<div class="ibox-content">
		<div id="divtbrespon"> 
            <table class="table table-bordered" id="tbmain">
            <thead>
            <tr>
                <th>Año</th>
                <th>Tipo</th>                
                <th>Total</th>                
            </tr>
            </thead>
            <tbody>
                <?php $total=0; ?>
            @foreach($procesados as $key => $value)        
                <tr>
                    <td>{{ $value->anio }}</td>
                    <td>{{ $value->tipo }}</td>
                    <td>{{ $value->total }}</td>                    
                </tr>
                <?php $total+=$value->total; ?>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th colspan="2">TOTAL</th>                
                <th>{{ $total }}</th>                
            </tr>
            </tfoot>
            </table> 
        </div>                                        
    </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@stop