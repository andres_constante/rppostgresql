<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
<script type="text/javascript" src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
$(document).ready(function() {
  $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
    @if(isset($validarjs))
    //Validacion
    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.",
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
      $('form').validate({       
          rules: {
            @foreach($validarjs as $key)
              {!! $key !!},
            @endforeach
            /*
              clave: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              },
              valor: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              }
            */
          },
          highlight: function(element) {
              $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
                  error.insertAfter(element.parent());
              } else {
                  error.insertAfter(element);
              }
          }
    @if(isset($popup))  
          ,
          submitHandler : function(form) {
          //do something here
              var frm = $('#form1');
              frm.submit(function (ev) {
                ev.preventDefault(); 
                //$("#divconsulta").html($("#gif_cargando").html());
                //$("#gif_cargando")
                $.ajax({
                    type: frm.attr('method'),
                    url: frm.attr('action'),
                    data: frm.serialize(),
                    statusCode: {
                        404: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 1 :(");
                        //bloquearpantalla(0);                                
                        },                            
                        401: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                        //bloquearpantalla(0);
                        }
                        //$("#divconsulta").html("Se produjo un error al consultar los datos. 4 :( ");
                    },
                    error: function(objeto, opciones, quepaso){
                        //setTimeout(function(){ajaxres.abort();}, 100);
                        //bloquearpantalla(0);
                        //$(tr).attr( "class","danger" );
                        //$("#divconsulta").html(quepaso);
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                    },
                    success: function (data) {       
                      console.log(data);
                      toastr[data[0]](data[1]);
                      parent.jQuery.colorbox.close();
                      /*
                      $("#divconsulta").html(""); 
                      $("#tipobuscar").val("");
                      if($.isArray(data)){
                        //console.log("Es array");
                        toastr["error"](data[1]);

                        $("#divconsulta").html(data[2]);
                      }
                      else{
                        var tipo = $('input:radio[name=radio]:checked').val();             
                        if(tipo==2) {
                          $("#divpropiedades").html(data);
                          $("#divconsulta").text("");
                        }
                        else{
                            $("#divconsulta").html(data);                      
                        }
                    }
                    */
                }
            });
            //bloquearpantalla(0);
            }); 
        }
        @endif
      });
@endif
$(".chosen-select").chosen();
  //Fin Validacion
 $('.selective-normal').selectize();
 $('.selective-tags').selectize({
          plugins: ['remove_button'],
          persist: false,
          create: true,
          render: {
            item: function(data, escape) {
              return '<div>"' + escape(data.text) + '"</div>';
            }
          },
          onDelete: function(values) {
            //return confirm(values.length > 1 ? 'Esta seguro de borrar la selección ' + values.length : '');
            return confirm('Esta seguro de borrar la selección?');
          }
        });
});
</script>
<script>
$( document ).ready(function() {
    $('#numeroespecie').on('input', function () { 
    this.value = this.value.replace(/[^0-9]/g,'');
    });
    $('#valor').on('input', function () { 
      this.value = this.value.replace(/[^0-9.]/g,'');  
    });
//Cargar deuda del Comerciante
  $("#idcomerciante").change(function(){
    var id = $(this).val();
    //alert(valor);
    var ruta ='{{ URL::to('deudacontribuyente') }}';
    $.get(ruta,
              { id: id },
                function(data) {  
                    //console.log(data);  
                    $("#divresul").html("<h3>Tiene una deuda pendiente de: <strong>$"+data+"</strong><h3>");
            });
  });
  $("#idseccion").change(function(){
    var id = $(this).val();
    //alert(valor);
    var ruta ='{{ URL::to('valortasa') }}';
    $.get(ruta,
              { id: id },
                function(data) {  
                    //console.log(data);  
                    $("#valor").val(data);
            });
  });
  });
</script>
<style>
  
@media screen and (max-width: 600px) {
  .tituprin {
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    text-transform:uppercase;
  }
}
</style>
@stop
@section ('contenido')
@if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif   
<h1 class="tituprin"> 
    @if($configuraciongeneral[2]=="crear")
       Nuevo Registro - {!!  $configuraciongeneral[0]  !!}
    @else
        Editar Registro - {!!  $configuraciongeneral[0]  !!}
    @endif
</h1>

<div class="ibox float-e-margins">
     <div class="ibox-title">
      <div class="">
        <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Todos</a>
        <a  href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                      
      </div>
<!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->all())
        <script>    
        $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
        toastr["error"]($("#diverror").html());    
        });
        </script>
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Error!</h4>
          <div id="diverror">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
        </div> 
        </div>
    @endif  
    @if(isset($alerta) && $alerta!="")
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
            <ul><li>{!!  $alerta  !!}</li></ul>
        </div>
    @endif
 </div>

<div class="ibox-content">
    
    
    @if($configuraciongeneral[2]=="crear")
     {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal'))  !!}
    @else
      {!!  Form::model($tabla, array('route' => array($configuraciongeneral[1].'.update', $tabla->id), 'method' => 'PUT','class'=>'form-horizontal', 'id'=>'form1' ))  !!}
    @endif
    

         @foreach($objetos as $i => $campos)
                <div class="form-group">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                  <div class="col-lg-8"> 
                      @if($campos->Tipo=="date")
                          <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                        {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                      @elseif($campos->Tipo=="datetext")
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, $campos->Valor , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                      @elseif($campos->Tipo=="datetextdisabled")
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, $campos->Valor , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                      @elseif($campos->Tipo=="select")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                      @elseif($campos->Tipo=="select2")
                         {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                      @elseif($campos->Tipo=="select-multiple")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple'))!!}
                      @elseif($campos->Tipo=="file") 
                          {!! Form::file($campos, array('class' => 'form-control')) !!}
                      @elseif($campos->Tipo=="textdisabled")
                         {!!   Form::text($campos->Nombre,$campos->Valor, array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                      @elseif($campos->Tipo=="textarea")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                            
                      @elseif($campos->Tipo=="password")
                         {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                      @else
                          {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      @endif
                  </div>
                </div>
        
        @endforeach
        @if(isset($popup)) 
          <input type="hidden" value="popup" name="txtpopup" />
        <!--else -->
          <!--input type="hidden" value="" name="txtpopup"/-->
        @endif
        {!!  Form::submit("Guardar", array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
        
        {!!  Form::close()  !!}
  <div id="divresul"></div>
  </div> 
  
</div> <!-- ibox float-e-margins -->

<script>
</script>    

@stop

