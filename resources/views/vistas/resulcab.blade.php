<script>
    $( document ).ready(function() {
        $('#tbmain').DataTable({
        responsive : true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
        "order": ([ 0, 'desc' ]),
        dom: 'Bflipt',
        buttons: [
            "copy","pdf","print"
        ]
    });//Datatable
    });
</script>
        <h2 id="proce" style="display:none;">Procesados {{ $totalproce }} de {{ $totalmain}} => {{ $porcentaje }}% => Pendientes: {{ $totalpendiente }}</h2>
            <div id="progreso" style="display:none;">
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated progress-bar-danger" style="width: {{ $porcentaje }}%" role="progressbar" aria-valuenow="{{ $porcentaje }}" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
    <table class="table table-bordered" id="tbmain">
    <thead>
    <tr>
        <th>ID</th>
        <th>Estado</th>
        <th>Observación</th>
        <th>ID Insert Ok</th>
        <th>Fecha</th>
    </tr>
    </thead>
    <tbody>
    @foreach($procesados as $key => $value)        
        <tr {{ ($value["estado"]!="PROCESADO"  ? 'class=bg-danger' : '') }}>
            <td>{{ $value["id"] }}</td>
            <td>{{ $value["estado"] }}</td>
            <td>{{ $value["detalleproceso"] }}</td>
            <td>{{ $value["idokmain"] }}</td>
            <td>{{ $value["fechaproceso"] }}</td>
        </tr>
    @endforeach
    </tbody>
    </table>    
