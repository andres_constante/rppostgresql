<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
 <script>
        $(document).ready(function() {
          function Imprime(obj)
          {
            var clon=$(obj).clone();
            clon = clon.find("a").remove().end();
            clon.find("#divtbrespon").html(clon.find(".table-responsive").html());

            clon = clon.find(".table-responsive").remove().end();

            var contents = clon.html();
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write('<html><head><title>DIV Contents</title>');
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            //frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            $("link, style").each(function() {
              //$(printWindow.document.head).append($(this).clone())
              var estilo=$(this);
              //console.log(estilo[0].href);
              if(estilo[0].href!=undefined)
              frameDoc.document.write('<link href="'+estilo[0].href+'" rel="stylesheet" type="text/css" />');
            });
            //Append the DIV contents.
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);
          }
function imprimirelem(obj){ // No funciona esta impresion
    var printWindow = window.open("", "Print" );

    $("link, style").each(function() {
      $(printWindow.document.head).append($(this).clone())
      var estilo=$(this);
      console.log(estilo[0].href);
    });
    var toInsert = $(obj).clone();//.html();
    //toInsert= toInsert.find('a').remove().end();
    /*toInsert= toInsert.find('select').remove().end();
    toInsert= toInsert.find('.chosen-container').remove().end();
    toInsert= toInsert.find('.aremove').remove().end();
    toInsert= toInsert.find('.editorBtn').remove().end();
    //toInsert.find('.diveditable').css("background-color","red");
    toInsert.find('.diveditable').removeClass("diveditable");
    toInsert.find('#htitulo').remove();
    */
    /*toInsert.find("#tbbuzonmain tr").each(function() {
      $(this).find("td:eq(7)").remove();
      $(this).find("th:eq(7)").remove();
    });*/
    $(printWindow.document.body).append(toInsert);
    //alert("Hola");
    setTimeout(function(){ printWindow.print(); },500);
    //printWindow.close();
  }
            $("#btnimprimir").click(function (){
              return Imprime("#divimprimir");
            });
            $('#tbbuzonmain').dataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]]),
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                    //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                    //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf
                  
            });            
        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        });  
@if(isset($delete))
    function eliminar($id)
    {
            var r= confirm("Seguro de eliminar este registro?");
            if(r==true)                        
                $('#frmElimina'+$id).submit();
            else
                return false;
    }
@endif
@if(isset($validarjs))
@if(count($validarjs))
    //Validacion
    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.",
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
      $('form').validate({       
          rules: {
            @foreach($validarjs as $key)
              {!! $key !!},
            @endforeach
            /*
              clave: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              },
              valor: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              }
            */
          },
          highlight: function(element) {
              $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
                  error.insertAfter(element.parent());
              } else {
                  error.insertAfter(element);
              }
          }
      });
@endif
@endif

</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF"> {{ $configuraciongeneral[0] }}</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif                          
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{{ URL::to($configuraciongeneral[1]) }}">Listar Todos</a>
                                </li>
                               
                                <li><a id="btnnuevo" href="{{ URL::to($configuraciongeneral[1]."/create") }}">Nuevo</a>
                                </li>
                               
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                     @if (isset($configuraciongeneral[3]))   
                      {{Form::open(array('method' => 'PUT','action' => $configuraciongeneral[3],'role'=>'form' ,'class'=>'form-inline','id'=>'formu')) }}
                           <div class="row"> 
                               <div class="col-md-12">
                                    <div class="col-md-6">        
                                        {{ Form::label($configuraciongeneral[1].'bus', 'Filtrar Busqueda por:',array('class'=>'control-label')) }}          
                                        {{ Form::select($configuraciongeneral[1].'bus',[null=>'Escoja'] + $listabusqueda ,$valorcmb,array('class' => 'chosen-select','style' => 'width:90%')) }}  <br> <br> 
                                    </div>
                                    <div class="col-md-3">
                                        {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}
                                    </div>    
                             </div>
                          </div>
                        {{Form::hidden('ruta',URL::to('pensionrecaudacion') ,array('id'=>'ruta'))}}
                        {{ Form::close() }} 
                     @endif   
                      <div class="">
                       <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Todos</a>
                       
                        <a  href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                        <a  href="javascript:" class="btn btn-default" id="btnimprimir">Imprimir</a>
                      
                      </div>
                <div id="divtbrespon"> </div>
                <div class="table-responsive">
                <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>           
                           <th>ID</th>
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </thead>                    
                    <tfoot>
                        <tr>            
                          <th>ID</th>
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </tfoot>                                        
                    <tbody>

                    @foreach($tabla as $key => $value)
                    <tr>
                         <td>{{ $value->id }} </td> 
                        @foreach($objetos as $keycam => $valuecam)                        
                               <td>
                               <?php
                                    $cadena="echo trim(\$value->".$valuecam->Nombre.");";                                    
                                ?>
                                    @if($valuecam->Nombre=="valor_predefinido")
                                        <textarea style="margin: 0px; width: 343px; height: 185px;" disabled>
                                            <?php eval($cadena); ?>
                                        </textarea>
                                    @else
                                        <?php eval($cadena); ?>
                                    @endif
                               </td>
                        @endforeach
                        <td>
                        <!--      Cuando es vista de muchos botones -->
                        @if (isset($botonruta))
                               <div class="btn-group">
                                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Editar <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                    @foreach($botonruta as $keyboton => $botonesvalue)
                                         <?php 
                                            eval("\$cade=\$value->".$botonvariable[$keyboton].";");
                                            $rutaboton = str_replace("@variable",$cade, $botonesvalue);
                                         //$rutaboton = str_replace("@variable", $value->$botonvariable[$keyboton], $botonesvalue);
                                         ?>
                                            <li><a href="{{ URL::to($rutaboton)}}" target="_blank" class="font-bold">{{ $botoncaption[$keyboton] }} </a></li>
                                            <li class="divider"></li>
                                    @endforeach   
                                       </ul>
                               </div>
                        @else
                            <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id) }}" class="divpopup" target="_blank">
                                <i class="fa fa-newspaper-o"></i></a>
                            <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id."/edit") }}"><i class="fa fa-pencil-square-o"></i></a>
                            @if(isset($delete))
                                <a href="javascript::" onclick="eliminar({{ $value->id }})"><i class="fa fa-trash"></i> </a>
                            @endif
                        @endif
                        </td>
                    </tr>
                      @endforeach                      
                     </tbody>
                           
                </table>  
                </div><!-- Tbale responsive --> 
                           @if(isset($sumatotal))
                    <table class="table table-striped table-bordered table-hover display">
                    <tr>
                    <th>
                      <h3><strong>TOTAL RECAUDADO: ${{ $sumatotal }}</strong></h3>
                    </th>
                    </tr>
                    </table>

                  @endif  
                        {{-- $tabla->links() --}}
                </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@if(isset($delete))
    <div style="display: none;">
    @foreach($tabla as $key => $value)
        {!! Form::open(['route' => [$configuraciongeneral[1].'.destroy', $value->id], 'method' => 'delete','id'=>'frmElimina'.$value->id,'class' => 'pull-right']) !!}
            {!! Form::submit('Eliminar', array('class' => 'btn btn-small btn-warning')) !!}
        {!! Form::close() !!}
    @endforeach
    </div>
@endif
@stop
