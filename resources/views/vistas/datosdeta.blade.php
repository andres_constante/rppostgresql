@extends ('layout')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
<script>
$( document ).ready(function() {
        $('#tbmain').DataTable({
        responsive : true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
        "order": ([ 0, 'desc' ]),
        dom: 'Bflipt',
        buttons: [
            "copy","pdf","print"
        ]
    });//Datatable
    });
</script>
@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF">Intervinientes Migrar</h1>
<div class="ibox float-e-margins">
	<div class="ibox-title"> 
    	@if (Session::has('message'))
    		<script>    
			$(document).ready(function() {
    			//toastr.succes("{{ Session::get('message') }}");
			    toastr["success"]("{{ Session::get('message') }}");
			    //$.notify("{{ Session::get('message') }}","success");
			});
			</script>    
     		<div class="alert alert-info">{{ Session::get('message') }}</div>
    	@endif                          
			<!--div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
            </div-->		
	</div>
	<div class="ibox-content">
		<div id="divtbrespon"> 
            <table class="table table-bordered" id="tbmain">
            <thead>
            <tr>
                <th>ID</th>   
                <th>ID Inscripción</th>             
                <th>Calidad Comparece</th>
                <th>Cédula</th>
                <th>Nombre</th>
                <th>Tipo Cliente</th>                
                <th>Estado Civil</th>                
                <th>Estado</th>
                <th>Detalle</th>
                <th>Fecha</th>                
            </tr>
            </thead>
            <tbody>
            @foreach($procesados as $key => $value)        
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->idcab }}</td>
                    <td>{{ $value->calidad_comparece }}</td>
                    <td>{{ $value->cedula }}</td>
                    <td>{{ $value->nombre_razon }}</td>
                    <td>{{ $value->tipo_cliente }}</td>                    
                    <td>{{ $value->estado_civil }}</td>                    
                    <td>{{ $value->estado }}</td>
                    <td>{{ $value->detalleproceso }}</td>
                    <td>{{ $value->fechaproceso }}</td>
                    
                </tr>
            @endforeach
            </tbody>
            </table> 
        </div>                                        
    </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@stop