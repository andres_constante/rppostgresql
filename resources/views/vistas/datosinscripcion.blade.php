@extends ('layout')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
<script>
$( document ).ready(function() {
        $('#tbmain').DataTable({
        responsive : true,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
        "order": ([ 0, 'desc' ]),
        dom: 'Bflipt',
        buttons: [
            'print','copy', 'excel', 'pdf'
        ]
    });//Datatable
    });
</script>
@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF">Inscripciones</h1>
<div class="ibox float-e-margins">
	<div class="ibox-title"> 
    	@if (Session::has('message'))
    		<script>    
			$(document).ready(function() {
    			//toastr.succes("{{ Session::get('message') }}");
			    toastr["success"]("{{ Session::get('message') }}");
			    //$.notify("{{ Session::get('message') }}","success");
			});
			</script>    
     		<div class="alert alert-info">{{ Session::get('message') }}</div>
    	@endif                          
			<!--div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
            </div-->
            {{ Form::open(array('url' => 'reporteingresos','role'=>'form' ,'class'=>'form-horizontal')) }}
<div class="panel-group" id="accordion" style="width: 400px;">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" >
                    <i class="fa fa-search fa-fw"></i>Filtro de Fechas</a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse {{ $colapse }}">
        <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>Desde:</th><td>{{ Form::input('date', 'desde', $fecha1, ['class' => 'form-control', 'placeholder' => 'Date']) }}</td>
                    </tr>
                    <tr>
                    <th>Hasta:</th><td>{{ Form::input('date', 'hasta', $fecha2, ['class' => 'form-control', 'placeholder' => 'Date']) }}</td>
                        <td rowspan="2"> {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}</td>
                    </tr>                                        
                </table>
        </div>
        </div>
    </div>                            
</div>

{{ Form::close() }}		
	</div>
	<div class="ibox-content">
		<div id="divtbrespon"> 
            <table class="table table-bordered" id="tbmain">
            <thead>
            <tr>                
                <th>ID Inscripción</th>
                <th>Fecha Proceso</th>
                <th>Fecha Inscripción</th>
                <th>Libro</th>
                <th>Acto</th>                
                <th>Número Inscripción</th>
                <th>Número Repertorio</th>                
                <th>Creado por</th>                                
            </tr>
            </thead>
            <tbody>
            @foreach($procesados as $key => $value)        
                <tr>
                    <td>{{ $value->OID }}</td>
                    <td>{{ $value->fecha }}</td>
                    <td>{{ $value->FechaInscr }}</td>
                    <td>{{ $value->DscaLibro }}</td>
                    <td>{{ $value->DescActo }}</td>
                    <td>{{ $value->NroInscripcion }}</td>
                    <td>{{ $value->NroRepertorio }}</td>                    
                    <td>{{ $value->CreadoPor }}</td>                    
                </tr>
            @endforeach
            </tbody>
            </table> 
        </div>                                        
    </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@stop