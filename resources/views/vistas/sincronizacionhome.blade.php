@extends ('layout')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
<script>
   function procesacabecera(){ 
        if($("#chkok").is(':checked'))
            return false;
        $("#divtbrespon").html("<div class='jumbotron'><h3>Espere por favor</h3><img src='{{ asset("img/cargando.gif")}}'></div>");
        $("#divchk").show();        
            $.ajax({
                   type: "GET",
                   url: '{{ URL::to("procesardatoscab") }}',
                   /*data: { id_area_materia:id1, id_tipo_educacion:id2,_token:'ssgd'},*/
                   statusCode: {
                       404: function() {
                       toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :(");
                       //bloquearpantalla(0);                                
                        $("#divtbrespon").html("Se produjo un error página no encontrada, disculpe las molestias... :(");
                       },                            
                       401: function() {
                       toastr["error"]("Inicie sesión de nuevo por favor... :(");
                       //bloquearpantalla(0);
                        $("#divtbrespon").html("Se produjo un error, disculpe las molestias... :(");
                       }
                   },
                   error: function(objeto, opciones, quepaso){
                       //setTimeout(function(){ajaxres.abort();}, 100);
                       //bloquearpantalla(0);
                       //$(tr).attr( "class","danger" );
                        $("#divtbrespon").html("Se produjo un error, disculpe las molestias... :("+quepaso);
                   },
                   success: function( msg ) {               
                       //alert(res);
                        //console.log(msg);
                        if(parseInt(msg)!=1)
                        {
                            $("#divtbrespon").html(msg);
                            $("#divproce").html($("#proce").html()+$("#progreso").html());
                            setTimeout(function(){
                                procesacabecera();
                            }, 3000);
                        }else{
                            $("#divtbrespon").html("<h2>No hay nada para procesar.</h2>");
                        }
                   }
               });
}
</script>
@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF"> Sincronización de Datos - Inscripciones</h1>
<div class="ibox float-e-margins">
	<div class="ibox-title"> 
    	@if (Session::has('message'))
    		<script>    
			$(document).ready(function() {
    			//toastr.succes("{{ Session::get('message') }}");
			    toastr["success"]("{{ Session::get('message') }}");
			    //$.notify("{{ Session::get('message') }}","success");
			});
			</script>    
     		<div class="alert alert-info">{{ Session::get('message') }}</div>
    	@endif                          
			<!--div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
            </div-->
		<a  href="javascript:procesacabecera()" class="btn btn-primary ">Inciar Proceso</a><br><div id="divchk" style="display:none;"><input type="checkbox" name="chkok" id="chkok" value="0">	Detener Ejecución</div>
        <br><div id="divproce"></div>
	</div>
	<div class="ibox-content">
		<div id="divtbrespon"> </div>                                        
    </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@stop