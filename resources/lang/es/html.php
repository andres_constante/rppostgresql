<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */
    'main' => [
        'sistema' => 'Sistema Principal',
        'sistemasub' => 'Sistema Principal Sincronización de Inscripciones',
        'copyright' => 'Copyright &copy; Andrés Constante'
    ],
];
