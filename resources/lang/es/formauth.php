<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */    
    'login' => [
    	'title'		=> 'Acceso al sistema',
        'titlesub'     => 'Por favor ingrese su Email y Contraseña para empezar.',
    	'submit'	=> 'Entrar',
    	'registro'  => 'Primera vez en el sistema?',
        'olvido'  => 'Olvidó su contraseña?',
        'inicio' => 'Página Principal'
    ],
    'registro' => [
    	'title'		=> 'Nuevo Usuario',
        'titlesub'     => 'Por favor ingrese su nombre, email y contraseña.',
    	'submit' 	=> 'Crear usuario'
    ],
    'olvido' => [
        'title'     => 'Nuevo Usuario',
        'titlesub'     => 'Por favor ingrese su nombre, email y contraseña.',
        'submit'    => 'Crear usuario'
    ],
    'reset' => [
        'title'     => 'Crear nueva contraseña',
        'titlesub'     => 'Por favor ingrese su email y contraseña.',
        'submit'    => 'Restablecer contraseña'
    ],
    'email' => [
        'title'     => 'Restablecer tu contraseña',
        'titlesub'     => 'Por favor ingrese el Email para enviar el link de reseteo.',
        'submit'    => 'Enviar Link para Restablecer Contraseña'
    ],
    	
    'label'	=> [
    	'email'					=> 'Email: email@server.com',
    	'password'				=> 'Contraseña',
    	'password_confirmation' => 'Confirmar contraseña',
    	'remember'				=> 'Recuerdame',
    	'name'					=> 'Nombres',
        'cedula'                => 'Cédula'
    ]
];
